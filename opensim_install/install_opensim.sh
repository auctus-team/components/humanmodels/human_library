#!/bin/bash

# If you only want to install a released version of Opensim.
# Indicates the path where Opensim should be installed (the path where install_opensim.sh should be).
FOLDER_PATH=$PWD

sudo apt install build-essentials git cmake openjdk-8-jdk liblapack3 libgconf-2-4

if [ ! -f opensim-core-latest_linux_Release.zip ]; then
   wget https://prdownloads.sourceforge.net/myosin/opensim-core/opensim-core-latest_linux_Release.zip
fi
if [ ! -f opensim-core-latest_linux_Release.zip ]; then
   echo "Error. File was not downloaded. You may have to try to download it yourself, then save it into this folder."
   exit 1
else
   unzip -q opensim-core-latest_linux_Release.zip -d $FOLDER_PATH
fi

