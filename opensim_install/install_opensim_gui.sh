#!/bin/bash

# If you want to install Opensim Gui. I have not been able to make it work, but perheaps you may be luckier than me?

# Indicates the path to home (where netbeans-8.2 should be installed).
HOME_PATH="/home/elandais"
# Indicates the path where Opensim should be installed (the path where install_opensim_gui.sh should be).
FOLDER_PATH=$PWD

sudo apt install build-essentials git cmake openjdk-8-jdk liblapack3 libgconf-2-4

if [ ! -d $HOME_PATH"/netbeans-8.2" ]; then
  wget https://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-javase-linux.sh
  chmod 755 netbeans-8.2-javase-linux.sh
  echo -e "This command will launch the installation software for Netbeans 8.2 IDE. Change only the JAVA platform (JDK installation) for the path to Java 8 (/usr/lib/jvm/java-8-openjdk-amd64 normally).\nNetbeans 8.2 IDE should be installed in ~"
  ./netbeans-8.2-javase-linux.sh 
fi

read -p 'Opensim-core already installed from source : [Y/N]' Src
#Src="$Src"
if [ $Src = "Y" ] || [ $Src = "y" ]; then
   read -p "Please give the complete path to the install" SRC_PATH
elif [ $Src = "N" ] || [ $Src = "n" ]; then
   if [ ! -f opensim-core-latest_linux_Release.zip ]; then
       wget https://prdownloads.sourceforge.net/myosin/opensim-core/opensim-core-latest_linux_Release.zip
   fi
   if [ ! -f opensim-core-latest_linux_Release.zip ]; then
       echo "Error. File was not downloaded. You may have to try to download it yourself, then save it into this folder."
       exit 1
   else
       unzip -q opensim-core-latest_linux_Release.zip -d $FOLDER_PATH
       SRC_PATH=$FOLDER_PATH"/opensim-core"
   fi
else
  echo "No interesting response."
  exit 1
fi

# other possibility, cleaner but longer (also install Opensim-API, useless in this config)
#git clone --recurse-submodules https://github.com/opensim-org/opensim-gui.git
#cd opensim-gui
#rm -r OpenSimAPI

git clone https://github.com/opensim-org/opensim-gui.git
cd opensim-gui
git clone https://github.com/opensim-org/opensim-models.git
git clone https://github.com/opensim-org/opensim-visualizer.git
cd Gui/opensim
rm -r threejs
git clone https://github.com/opensim-org/three.js.git
mv three.js threejs

cd $FOLDER_PATH
sudo rm -r build
mkdir build
cd build

cmake ../opensim-gui -DCMAKE_PREFIX_PATH=$SRC_PATH \
    -DAnt_EXECUTABLE="~/netbeans-8.2/extide/ant/bin/ant" \
    -DANT_ARGS="-Dnbplatform.default.netbeans.dest.dir="$HOME_PATH"/netbeans-8.2;-Dnbplatform.default.harness.dir="$HOME_PATH"/netbeans-8.2/harness;-Dnbplatform.default.cluster.path="$HOME_PATH"/netbeans-8.2/moduleCluster.properties"

make CopyOpenSimCore
make PrepareInstaller

# Install the Opensim-Gui into OpenSim-Gui-Install
cd $FOLDER_PATH/opensim-gui/Gui/opensim/dist/installer/OpenSim
chmod +x INSTALL
./INSTALL --verbose -p $FOLDER_PATH/OpenSim-Gui-Install 

# Add the paths to the Simbody libraries.
cd /etc/ld.so.conf.d
sudo su -c "echo -e '$FOLDER_PATH/OpenSim-Gui-Install/sdk/Simbody/lib\n$FOLDER_PATH/OpenSim-Gui-Install/sdk/lib\n/usr/lib/x86_64-linux-gnu' > 99local.conf"
sudo ldconfig 

cd $FOLDER_PATH
echo "Everything should be good. Give it a try with OpenSim-Gui-Install/bin/OpenSim!"

