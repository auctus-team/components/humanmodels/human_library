#include "human_library/fpin.h"


namespace fpin{

  /**
   * @brief Default constructor of the URDFModel object. Nothing is initialized here.
   * 
   */
  URDFModel::URDFModel()
  {
    
  }

  /**
   * 
   * @brief Complete initializer of the URDFModel object.
   * 
   * @param urdf_filename 
   */
  URDFModel::URDFModel(std::string urdf_filename):
    urdf_data()
  {
    pin::urdf::buildModel(urdf_filename,urdf_model);
    urdf_model.gravity.linear( Eigen::Vector3d(0,0,-9.81));
    pin::Data data(urdf_model);
    urdf_data = data;
    q = pin::neutral(urdf_model);
    D_joints_props = getJointsProperties(urdf_model);
    v= Eigen::VectorXd::Zero(urdf_model.nv);            

  }

  /**
   * @brief Set the joint configuration of the model to the neutral position.
   * 
   */
  void URDFModel::setNeutral()
  {
    q = pin::neutral(urdf_model);
  }

  /**
   * @brief Given a map joint_name:joints_values, set the values of the joints of the model.
   * 
   * @param D_j_v 
   */
  void URDFModel::forwardKinematics(std::map<std::string,std::vector<double> > D_j_v)
  {
    for (pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
    {
      std::string name = urdf_model.names[joint_id];
      if (D_j_v.find(name) != D_j_v.end() )
      {

        int nq = urdf_model.joints[joint_id].nq();
        int idx_q = urdf_model.joints[joint_id].idx_q();
        std::vector<double> value = D_j_v[name];
        int l = 0;
        for (int i = idx_q; i < idx_q+nq; i++)
        {
          q(i,0) = value[l];
          l++;
        }
      }
    }
    pin::forwardKinematics(urdf_model,urdf_data,q);
    pin::updateFramePlacements(urdf_model,urdf_data);
    
  }

  void URDFModel::setSpeed(std::map<std::string,std::vector<double> > D_j_s)
  {

    for (pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
    {
      std::string name = urdf_model.names[joint_id];
      if (D_j_s.find(name) != D_j_s.end() )
      {
        int nv = urdf_model.joints[joint_id].nv();
        int idx_v = urdf_model.joints[joint_id].idx_v();
        //std::cout << nv << " , " << idx_v << std::endl;        
        std::vector<double> value = D_j_s[name];
        int l = 0;
        for (int i = idx_v; i < idx_v+nv; i++)
        {
          v(i,0) = value[l];
          l++;
        }
      }
    }

  }

  // printConfig
  void printJointsPositions( pin::Model urdf_model, pin::Data urdf_data)
  {
    std::cout << "---- Translation ----" << std::endl;
    for(pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
    std::cout << std::setw(24) << std::left
                << urdf_model.names[joint_id] << ": "
                << std::fixed << std::setprecision(2)
                << urdf_data.oMi[joint_id].translation().transpose()
                << std::endl;
    std::cout << "---- Rotation ----" << std::endl;
    for(pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)    
    std::cout << std::setw(24) << std::left
                << urdf_model.names[joint_id] << ": "
                << std::fixed << std::setprecision(2)
                << (urdf_data.oMi[joint_id].rotation() ).eulerAngles(0,1,2).transpose()
                << std::endl;                
  }

  std::map<std::string,Eigen::Matrix4d> getJointsPositions(pin::Model urdf_model, pin::Data urdf_data)
  {
    std::map<std::string, Eigen::Matrix4d> D_j_pos;

    for(pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
    {
      std::string name = urdf_model.names[joint_id];
      Eigen::Matrix4d M; M.setIdentity();
      for (int i = 0; i < 3; i++)
      {
        for(int j = 0; j <3; j++)
        {
          M(i,j) = urdf_data.oMi[joint_id].rotation()(i,j);
        }
        M(i,3) = urdf_data.oMi[joint_id].translation()[i];
      }
      D_j_pos[name] = M;
      
    }
    return(D_j_pos);
  }

  std::map<std::string,Eigen::Matrix4d> getFramesPositions(pin::Model urdf_model, pin::Data urdf_data)
  {
    std::map<std::string, Eigen::Matrix4d> D_j_pos;

    for(int l= 0; l < urdf_model.nframes; l++)
    {
      std::string name = urdf_model.frames[l].name;
      //std::cout << "name : " << name << std::endl;
      Eigen::Matrix4d M; M.setIdentity();
      for (int i = 0; i < 3; i++)
      {
        for(int j = 0; j <3; j++  )
        {
          M(i,j) = urdf_data.oMf[l].rotation()(i,j);
        }
        M(i,3) = urdf_data.oMf[l].translation()[i];
      }
      //std::cout << "M : " << M << std::endl;
      D_j_pos[name] = M;
      
    }
    return(D_j_pos);
  }


  void printJointsValues(pin::Model urdf_model, Eigen::VectorXd q)
  {
     std::cout << "--- Joint values ----" << std::endl;
     for(pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
     {
        int nq = urdf_model.joints[joint_id].nq();
        int idx_q = urdf_model.joints[joint_id].idx_q();
        if (idx_q != -1)
        {
          std::string name = urdf_model.names[joint_id];     
          //std::cout << nq << " , " << idx_q << std::endl;           
          std::cout << name << " : [ ";

          for (int i = idx_q; i < idx_q+nq-1; i++)
          {
            std::cout << q(i,0) << " ; ";
          }
          std::cout << q(idx_q+nq-1,0) << " ]" << std::endl;          
        }

     }
  }

  void printJointsSpeeds(pin::Model urdf_model, Eigen::VectorXd v)
  {
     std::cout << "--- Joint speeds ----" << std::endl;
     for(pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
     {
        int nv = urdf_model.joints[joint_id].nv();
        int idx_v = urdf_model.joints[joint_id].idx_v();

        if (idx_v != -1)
        {        
          std::string name = urdf_model.names[joint_id];     
          std::cout << name << " : [ ";
          for (int i = idx_v; i < idx_v+nv-1; i++)
          {
            std::cout << v(i,0) << " ; ";
          }
          std::cout << v(idx_v+nv-1,0) << " ]" << std::endl;          
        }

     }
  }

  /**
   * @brief For the name of each joint, get its properties : idx_q (index in q), nq (size in q), idx_v (index in v), nv (size in v).
   * 
   * @param urdf_model 
   * @return std::map<std::string, std::vector<int> > 
   */
  std::map<std::string, std::vector<int> > getJointsProperties(pin::Model urdf_model)
  {
    std::map<std::string, std::vector<int> > D_joints_props;
    for(pin::JointIndex joint_id = 0; joint_id < (pin::JointIndex)urdf_model.njoints; ++joint_id)
    {
      int nq = urdf_model.joints[joint_id].nq();
      int idx_q = urdf_model.joints[joint_id].idx_q();
      int nv = urdf_model.joints[joint_id].nv();
      int idx_v = urdf_model.joints[joint_id].idx_v();      
      std::string name = urdf_model.names[joint_id]; 
      std::vector<int> props;
      props.push_back(idx_q);
      props.push_back(nq);
      props.push_back(idx_v);
      props.push_back(nv);
      D_joints_props[name] = props;
    }
    return(D_joints_props);
  }

  void printJointsProperties(pin::Model urdf_model)
  {
    std::map<std::string, std::vector<int> > D_joints_props = getJointsProperties(urdf_model);
    for (std::map<std::string,std::vector<int> >::iterator it = D_joints_props.begin(); it!= D_joints_props.end(); it++)
    {
      std::cout << it->first << " : ";
      std::cout << "idx_q : " << it->second[0] << " ; nq : " << it->second[1] << std::endl;
    }
  }  

}