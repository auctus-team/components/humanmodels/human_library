#include "human_library/scipy_signal.h"

namespace scipy_signal
{

    double eps = 1e-12;

    std::map<std::string,std::string> band_dict = 
    {
        {"band", "bandpass"},
        {"bandpass", "bandpass"},
        {"pass", "bandpass"},
        {"bp", "bandpass"},
        {"bandstop","bandstop"},
        {"bands","bandstop"},
        {"bs","bandstop"},
        {"stop","bandstop"},
        {"lowpass", "lowpass"},
        {"l", "lowpass"},
        {"low", "lowpass"},
        {"lp", "lowpass"},
        {"highpass","highpass"},
        {"high","highpass"},
        {"hp","highpass"},
        {"h","highpass"}
    };
    // band_dict[str("band")] = "bandpass";
    // band_dict[str("bandpass")] = "bandpass";    
    // band_dict[str("pass")] = "bandpass";        
    // band_dict[str("bp")] = "bandpass";    
    // band_dict[str("bs")] = "bandstop";    
    // band_dict[str("bandstop")] = "bandstop";    
    // band_dict[str("bands")] = "bandstop";    
    // band_dict[str("stop")] = "bandstop";            
    // band_dict[str("l")] = "lowpass";
    // band_dict[str("low")] = "lowpass";
    // band_dict[str("lowpass")] = "lowpass"; 
    // band_dict[str("lp")] = "lowpass";    
    // band_dict[str("high")] = "highpass";
    // band_dict[str("highpass")] = "highpass";    
    // band_dict[str("h")] = "highpass";   
    // band_dict[str("hp")] = "highpass"; 

    std::map<std::string,std::string> filter_dict =  {
        {"butter","butter"},
        {"butterworth","butter"}
    };    

    std::map<int,std::string> error_type={
        {0,"success"},
        {2,"ValueError"},
        {3,"NotImplementedError"},
        {4,"IndexError"}

    };

    int filtfilt(
        Eigen::VectorXd b,
        Eigen::VectorXd a,
        Eigen::MatrixXd x,        
        Eigen::MatrixXd & y,
        int axis,
        std::string padtype,
        int padlen,
        std::string method,
        int irlen
    )
    {
        if (method != "pad" && method != "gust")
        {
            std::cout << "method must be 'pad' or 'gust'." << std::endl;
            return(2);
        }

        if (method == "gust")
        {
            std::cout << "Gustaffson method not implemented yet." << std::endl;
            return(3);
        }

        Eigen::MatrixXd yc;
        int edge;
        Eigen::MatrixXd ext;

        int result = _validate_pad(padtype, padlen, x, axis, max(a.rows(), b.rows()), edge, ext );
        //std::cout << "validate ok" << std::endl;
        //std::cout << "ext : " << ext.rows() << std::endl;
        // ext ok
        if (result == 0)
        {
            Eigen::VectorXd zi;
            result = lfilter_zi(b,a,zi);
            //std::cout << "zi : " << zi << std::endl;
            if (result == 0)
            {
                std::vector<double> v{1.0,1.0};
                v[axis] = zi.rows();
                zi.conservativeResize(v[0],v[1]);
                //std::cout << "zi : " << zi << std::endl;
                // ok
                Eigen::MatrixXd x0;
                axis_slice(ext, x0, 0,1,1,axis);
                //std::cout << "x0 : " << x0 << std::endl;

                Eigen::MatrixXd ziX0 = zi*x0.transpose();
                //std::cout << "zix0 : " << ziX0 << std::endl;
                // ok
                Eigen::MatrixXd y1;
                
                result = lfilter(b,a,ext,ziX0,y1);
                //std::cout << "y1 : " << y1 << std::endl;
                //std::cout << y1.rows() << std::endl;
                // ok
                if (result == 0)
                {
                    Eigen::MatrixXd y0;
                    axis_slice(y1,y0,-1,-2);
                    //std::cout << "y0 : " <<y0 << std::endl;

                    Eigen::MatrixXd ziY0 = zi*y0.transpose();
                    //std::cout << "ziY0 : " << ziY0 << std::endl;

                    //std::cout << "reverse : " << y1.reverse() << std::endl;
                    // ok input
                    result = lfilter(b,a,y1.reverse(), ziY0, yc);
                    //std::cout << "y : " << yc << std::endl;  
                    // ok yc                  
                }
            }

        }

        if (result == 0)
        {   
            //std::cout << "y2 : " << yc << std::endl;       

            Eigen::MatrixXd y3 = yc.reverse();
  
            if (edge > 0)
            {

                axis_slice(y3,y,edge,-edge,1,axis);
                //std::cout << "y final : " << y << std::endl;  
            }
        }

        //std::cout << error_type[result] << std::endl;



        return(result);



    }

    /**
     * @brief 1-D only.
     * Based on https://scipy-dev.scipy.narkive.com/it3yWWFI/reimplementation-of-lfilter
     * 
     * N : number of datas
     * K : order of filter
     * 
     * @param b 
     * @param a 
     * @param x 
     * @param zi 
     * @param y 
     */
    int lfilter(
        Eigen::VectorXd b,
        Eigen::VectorXd a,
        Eigen::MatrixXd x,
        Eigen::VectorXd zi,
        Eigen::MatrixXd & y
    )
    {

        if (x.cols() > 1)
        {
            std::cout << "lfilter with more than 1-D not implemented" << std::endl;
            return(3);
        }

        Eigen::VectorXd pz = zi;
        Eigen::VectorXd z = pz;
        int N = x.rows();
        int K = a.rows()-1;
        //std::cout << "K : " << K << std::endl;
        y.resize(N,1);

        for (int n = 0; n < N; n++)
        {
            y(n,0) = x(n,0)*b(0,0) + pz(0,0);
    
            for (int k = 0; k < K-1; k++)
            {
                z(k,0) = b(k+1,0)*x(n,0)- a(k+1,0)*y(n,0) + pz(k+1,0);
            }

            z(K-1,0) = b(K,0)*x(n,0) - a(K,0)*y(n,0);
            //Eigen::VectorXd tmp = pz;
            pz = z;
            //z = tmp;
        }

        //std::cout << "z : " << z << std::endl;
        return(0);

    }

    int lfilter_zi(
        Eigen::VectorXd b,
        Eigen::VectorXd a,
        Eigen::VectorXd & zi
    )
    {
        int i =0;
        while (i < a.rows() && a[i,0] == 0.0)
        {
            i+=1;
        }
        if (i == a.rows())
        {
            std::cout << "There must be at least one nonzero `a` coefficient." << std::endl;
            return(2);
        }

        if (a[0]!= 1.0)
        {
            b = b/a(0,0);
            a = a / a(0,0);
        }
        int n = max(a.rows(), b.rows());
        int ar = a.rows();
        int br = b.rows();

        a.conservativeResize(n, 1);
        b.conservativeResize(n, 1);        
        for (int i = ar; i < n; i++)
        {
            a(i,0) = 0.0;
        }
        for (int i = br; i < n; i++)
        {
            b(i,0) = 0.0;
        }        

        // https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.companion.html

        Eigen::MatrixXd companion(a.rows() -1 , a.rows()-1);
        companion.setZero();
        for (int j = 1; j < a.rows() ; j++)
        {
            companion(0,j-1) = -a(j,0)/a(0,0);
            if (j < a.rows()-1 )
            {
                companion(j,j-1) = 1.0;
            }

        }
        //std::cout << "comp : " << companion.transpose() << std::endl;
        Eigen::MatrixXd eye(n-1,n-1 ); eye.setIdentity();

        Eigen::MatrixXd IminusA = eye - companion.transpose();


        Eigen::VectorXd B(n-1);
        for (int i = 0; i < n-1; i++)
        {
            B(i,0) = b(i+1,0) - a(i+1,0)*b(0,0);
        }
        //std::cout << "B : " << B << std::endl;
        zi.resize(n-1,1); zi.setZero();
        zi(0,0) = B.sum() / (IminusA.col(0).sum() );
        double asum = 1.0;
        double csum = 0.0;
        for (int k = 1; k < n-1; k++)
        {
            asum += a(k,0);
            csum += b(k,0) - a(k,0)*b(0,0);
            zi(k,0) = asum*zi(0,0) - csum;
        }

        return(0);

    }

    int even_ext(
        Eigen::MatrixXd x,
        int n,
        int axis,
        Eigen::MatrixXd & ext
    )
    {
        if (n<1)
        {
            ext = x;
            return(0);
        }
        else if (axis == 0)
        {
            if (x.rows() -1 < n)
            {
                std::cout << "The extension length n (" << n << ") is too big. It must not exceed x.shape[axis]-1, which is " << x.rows() -1 << std::endl;
                return(2);
            }
        }
        else if (axis == 1)
        {
            if (x.cols()-1 < n)
            {
                std::cout << "The extension length n (" << n << ") is too big. It must not exceed x.shape[axis]-1, which is " << x.cols() -1 << std::endl;
                return(2);
            }
        }
        Eigen::MatrixXd left_ext, right_ext;
        axis_slice(x,left_ext,n,0,-1,axis);
        axis_slice(x,right_ext, -2,-(n+2) ,-1,axis);

        if (axis == 0)
        {
            ext.resize( x.rows(), x.cols() + 2*(n)  );
            
        }
        else if (axis == 1)
        {
            ext.resize( x.rows()+2*n, x.cols() );
            
        }
        ext << left_ext, x, right_ext ;
        return(0);

    }

    int const_ext(
        Eigen::MatrixXd x,
        int n,
        int axis,
        Eigen::MatrixXd & ext
    )
    {
        if (n<1)
        {
            ext = x;
            return(0);
        }

        Eigen::MatrixXd left_end, left_ext, right_end, right_ext;
        axis_slice(x,left_end, 0,1,1,axis);

        axis_slice(x,right_end, -1,-2,-1,axis);

        if (axis == 0)
        {
            ext.resize( x.rows(), x.cols() + 2*(n)  );
            left_ext.resize(x.rows(),  n);
            right_ext.resize(x.rows(),  n);           
            for (int i = 0; i < n; i++)
            {
                left_ext.col( i  ) = left_end;
                right_ext.col( i  ) = right_end;
            }
        }
        else if (axis == 1)
        {
            ext.resize( x.rows()+ 2*n, x.cols()   );
            left_ext.resize(n, x.cols() );
            right_ext.resize(n, x.cols() );           
            for (int i = 0; i < n; i++)
            {
                left_ext.row( i  ) = left_end;
                right_ext.row( i  ) = right_end;
            }
        }
        ext << left_ext, x, right_ext ;
        return(0);

    }

    int odd_ext(
        Eigen::MatrixXd x,
        int n,
        int axis,
        Eigen::MatrixXd & ext
    )
    {
        if (n<1)
        {
            ext = x;
            return(0);
        }
        else if (axis == 0)
        {
            if (x.rows() -1 < n)
            {
                std::cout << "The extension length n (" << n << ") is too big. It must not exceed x.shape[axis]-1, which is " << x.rows() -1 << std::endl;
                return(2);
            }
        }
        else if (axis == 1)
        {
            if (x.cols()-1 < n)
            {
                std::cout << "The extension length n (" << n << ") is too big. It must not exceed x.shape[axis]-1, which is " << x.cols() -1 << std::endl;
                return(2);
            }
        }

        Eigen::MatrixXd left_end, left_ext, right_end, right_ext;
        axis_slice(x,left_end, 0,1,1,axis);
        //std::cout << "left_end : " << left_end << std::endl;        
        axis_slice(x,left_ext,n,0,-1,axis);

        //std::cout << "left_ext : " << left_ext << std::endl;   

        Eigen::MatrixXd onesL = left_ext; onesL.setOnes();

        onesL= onesL * left_end.transpose() ;

        axis_slice(x,right_end, -1,-2,-1,axis);
        //std::cout << "right_end : " << right_end << std::endl;      
        axis_slice(x,right_ext, -2,-(n+2) ,-1,axis);
        //std::cout << "right_ext : " << right_ext << std::endl;
        Eigen::MatrixXd onesR = right_ext; onesR.setOnes();
        onesR= onesR*right_end.transpose();

        if (axis == 1)
        {
            ext.resize( x.rows(), x.cols() + 2*(n)  );
            
        }
        else if (axis == 0)
        {
            ext.resize( x.rows()+2*n, x.cols() );
            
        }
        //std::cout << "left : " << 2*onesL-left_ext << std::endl;
        ext << 2*onesL-left_ext, x, 2*onesR-right_ext ;
        return(0);

    }


    void axis_slice(
        Eigen::MatrixXd a,
        Eigen::MatrixXd & out,
        int start,
        int stop,
        int step,
        int axis
    )
    {
        if (stop == INFINITY)
        {   
            if (axis == 0)
            {
                stop = a.rows();
            }
            else
            {
                stop = a.cols();
            }

        }

        if (axis == 0)
        {

            if (start < 0)
            {
                start = a.rows()+start;
            }            
            if (stop < 0)
            {
                stop = a.rows()+stop;
            } 
            double cstop,cstart;
            if (stop > start)
            {
                cstop = stop;
                cstart = start;
            }
            else
            {
                cstop = start;
                cstart = stop;
            }
            

            double numf = ( abs(cstop)- abs(cstart)  )/(abs(step));
            int num = numf;
            if (num < numf)
            {
                num +=1;
            }
  
            out.resize( num  , a.cols());

            for (int i =0; i < num; i++)
            {
                for (int j = 0; j < a.cols(); j++)
                {
                    out(i,j) = a(start + i*step,j);
                }
            }
        }

        else if (axis == 1)
        {
            if (start < 0)
            {
                start = a.cols()+start;
            }            
            if (stop < 0)
            {
                stop = a.cols()+stop;
            }          

            double cstop,cstart;
            if (cstop > cstart)
            {
                cstop = stop;
                cstart = start;
            }
            else
            {
                cstop = start;
                cstart = stop;
            }
            
            double numf = ( abs(cstop)- abs(cstart)  )/(abs(step));
            int num = numf;
            if (num < numf)
            {
                num +=1;
            }


            out.resize( a.rows()  , num);

            for (int j = 0; j < num; j++) 
            {
                for (int i =0; i < a.rows(); i++)
                {
                    out(i,j) = a(i,start + j*step);
                }
            }
        }

    }


    int _validate_pad(
        std::string padtype,
        int padlen,
        Eigen::MatrixXd x,
        int axis, 
        int ntaps,
        int & edge,
        Eigen::MatrixXd & ext
    )
    {
        if (padtype != "even" && padtype != "odd" && padtype != "constant" && padtype != "")
        {
            std::cout << "Unknown value "<< padtype <<" given to padtype.  padtype "
                          "must be 'even', 'odd', 'constant', or None." << std::endl;
            return(2);
        }

        if (padtype == "")
        {
            padlen = 0;
        }

        if (padlen == -1)
        {
            edge = ntaps*3;
        }
        else
        {
            edge = padlen;
        }

        if (axis == 0)
        {
            if (x.rows() <= edge)
            {
                std::cout << "The length of the input vector x must be greater than padlen, which is " << edge << std::endl;
                return(2);
            }
        }
        else if (axis == 1)
        {
            if (x.cols() <= edge)
            {
                std::cout << "The length of the input vector x must be greater than padlen, which is " << edge << std::endl;
                return(2);
            }
        }

        if (padtype !="" && edge > 0)
        {
            if (padtype == "odd")
            {
                odd_ext(x,edge,axis,ext);
            }
            else if (padtype == "even")
            {
                even_ext(x,edge,axis,ext);
            }            
            else
            {
                const_ext(x,edge,axis,ext);
            }             
        }
        else
        {
            ext = x;
        }
        
        

    }

    int butter(int N, 
    Eigen::VectorXd Wn, 
    Eigen::MatrixXd & a,
    Eigen::MatrixXd & b,    
    std::string btype, 
    bool analog, 
    double fs )
    {
        std::string output = "ba";
        std::vector<Eigen::MatrixXcd> outRepres;
        int result = iirfilter(N, Wn, outRepres, nan(""), nan(""), btype, analog, "butter", output, fs);

        if (result == 0)
        {
            Eigen::MatrixXcd bc = outRepres[0];
            b.resize(bc.rows(), bc.cols() );

            for(int i = 0; i < bc.rows(); i++)
            {
                b(i,0) = std::real( bc(i,0) );
            }

            Eigen::MatrixXcd ac = outRepres[1];
            a.resize(ac.rows(), ac.cols() );            
            for(int i = 0; i < ac.rows(); i++)
            {
                a(i,0) = std::real( ac(i,0) );
            }            

        }

        return(result);
    }

    void lower(std::string & s)
    {
        std::for_each(s.begin(), s.end(), [](char & c) {
            c = ::tolower(c);
        });
    }

    int iirfilter(int N, 
    Eigen::VectorXd Wn,
    std::vector<Eigen::MatrixXcd> & outRepres, 
    double rp ,
    double rs ,
    std::string btype ,
    bool analog ,
    std::string ftype,
    std::string output,
    double fs  )
    {
        int result;
        lower(ftype);
        lower(btype);
        lower(output);
        
        if (!isnan(fs))
        {
            if (analog)
            {
                std::cout << "fs cannot be specified for an analog filter" << std::endl;
                result = 2;
                return(result);
            }

            Wn = 2*Wn/fs;
        }

        if (band_dict.find(btype) == band_dict.end() )
        {
            std::cout << btype << " is an invalid bandtype for filter." << std::endl;
            result = 2;
            return(result);
        }
        else
        {
            btype = band_dict[btype];
        }
        
        if ( filter_dict.find(ftype) == filter_dict.end() )
        {
            std::cout << ftype << " is is not a valid basic IIR filter (for now)." << std::endl;
            result = 2;
            return(result);
        }
        else
        {
            ftype = filter_dict[ftype];
        }
        

        if (output != "ba" && output !="zpk" && output !="sos")
        {
            std::cout << output << " is not a valid output form." << std::endl;
            result = 2;
            return(result);
        }

        if (!isnan(rp) && rp < 0)
        {
            std::cout << " passband ripple (rp) must be positive" << std::endl;
            result = 2;
            return(result);
        }

        if (!isnan(rs) && rs < 0)
        {
            std::cout << " stopband attenuation (rs) must be positive" << std::endl;
            result = 2;
            return(result);
        }

        Eigen::MatrixXcd z;
        Eigen::MatrixXcd p;
        Eigen::MatrixXcd k;
        int result_f;
            
        if (ftype == "butter")
        {
            result_f = buttap(N,z,p,k);
            // std::cout << "z : " << z << std::endl;
            // std::cout << "p : " << p << std::endl;
            // std::cout << "k : " << k << std::endl;            

        }

        else
        {
            std::cout << ftype << "not implemented in iirfilter"  << std::endl;
            result = 3;
            return(result);
        }

        if (result_f != 0)
        {
            return(result_f);
        }

        Eigen::VectorXd warped(Wn.rows(),1);

        if (!analog)
        {
            if ( (Wn.array() < 0).any() || ( (Wn.array() > 1).any() )    )
            {

                if (!isnan(fs))
                {
                    std::cout << "Digital filter critical frequencies must be 0 < Wn < fs/2 " << std::endl;
                }
                else
                {
                    std::cout << "Digital filter critical frequencies must be 0 < Wn < 1 " << std::endl;
                }
                result = 2;
                return(result);
                
            }
            fs = 2.0;

            for (int i = 0; i < Wn.rows(); i++)
            {
                warped(i,0) = 2 * fs * tan( M_PI* Wn(i,0) / fs); 
            }

        }
        else
        {
            warped = Wn;
        }


        if (btype == "lowpass" || btype == "highpass")
        {
            if (Wn.rows() > 1)
            {
                std::cout << "Must specify a single critical frequency Wn for lowpass or highpass filter" << std::endl;
                result = 2;
                return(result);
            }

            if (btype == "lowpass")
            {

                result_f = lp2lp_zpk(z,p,k,warped(0,0));
            // std::cout << "z_lp : " << z << std::endl;
            // std::cout << "p_lp : " << p << std::endl;
            // std::cout << "k_lp : " << k << std::endl;               

            }
            else if (btype == "highpass")
            {
                result_f = lp2hp_zpk(z,p,k,warped(0,0));
            }            

        }

        // else if (btype == "bandpass" || btype == "bandstop")
        // {
        //     if (Wn.rows() < 2) 
        //     {
        //         std::cout << "Wn must specify start and stop frequencies for bandpass or bandstop filter" << std::endl;
        //         return(4);
        //     }
        //     else
        //     {
        //         double bo = warped(1,0)- warped(0,0);
        //         double wo = sqrt(warped(1,0)*warped(0,0));


        //     }
            
        // }

        else
        {
            std::cout << btype << " not implemented (yet) in iirfilter." << std::endl;
            return(3);
        }

        if (result_f != 0)
        {
            return(result_f);
        }

        if (!analog)
        {
            // std::cout <<"fs : " << fs << std::endl;
            result_f = bilinear_zpk(z,p,k,fs);
            // std::cout << "z_bil : " << z << std::endl;
            // std::cout << "p_bil : " << p << std::endl;
            // std::cout << "k_bil : " << k << std::endl;            
            if (result_f != 0)
            {
                return(result_f);
            }            
        }

       

        if (output == "zpk")
        {
            outRepres.push_back(z);
            outRepres.push_back(p);
            outRepres.push_back(k);
        }
        else if (output == "ba")
        {
            // b : numerator ; a : denominator.
            Eigen::MatrixXcd a;
            Eigen::MatrixXcd b;
            bool ret_complex_conj = true;
            zpk2tf(z,p,k,b,a,ret_complex_conj);

            if (ret_complex_conj)
            {
                outRepres.push_back(b);
                outRepres.push_back(a);
            }
            else
            {
                std::cout << "Error : no conjugate complex" << std::endl;
                return(2);
            }
            

        }

        else if (output == "sos")
        {
            std::cout << "Second Order Section output not implemented yet!" << std::endl;
            return(3);
        }

        return(0);


    };


    int bilinear_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double fs)
    {
        int degree;
        int result = _relative_degree(z,p,degree);
        Eigen::MatrixXcd z_int = z;
        Eigen::MatrixXcd p_int = p;        

        if (result == 0)
        {
            double fs2 = 2.0*fs;
            Eigen::MatrixXcd fs2_z;
            Eigen::MatrixXcd fs2_p;

            fs2_z = z_int; fs2_z.setOnes(); fs2_z = fs2*fs2_z;
            z = ( (fs2_z+z_int).array() / ( (fs2_z-z_int).array() ) ).matrix();

            fs2_p = p_int; fs2_p.setOnes(); fs2_p = fs2*fs2_p;
            p = ( (fs2_p+p_int).array() / ( (fs2_p-p_int).array() ) ).matrix();

            int prev_row = z_int.rows();
            z.conservativeResize(z_int.rows()+degree,1);
            for (int i = prev_row; i < z.rows(); i++)
            {
                z(i,0) = -1.0;
            }

            k = k*real( (fs2_z-z_int).prod() / (fs2_p - p_int).prod()  );

        }
        return(result);
    }



    int buttap(int N, Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k)
    {
        if (abs(N) != N)
        {
            std::cout << "Filter order must be a nonnegative integer" << std::endl;
            return(2);
        }

        z.resize(0,0);
        p.resize(N,1);
        const std::complex<double> j(0, 1);        
        int m = -N+1;

        for (int i = 0; i < N; i++)
        {
            double val = M_PI*m/(2*N);
            p(i,0) = -std::exp( j*val);
            m+=2;
        }

        k.resize(1,1);
        k(0,0) = 1;
        return(0);
    }

    int lp2lp_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double wo)
    {
        int degree;
        int result = _relative_degree(z,p,degree);


        if (result == 0)
        {
            z = wo*z;
            p = wo *p;
            k = k*pow(wo,degree);

        }
        return(result);
        
    }


    int lp2hp_zpk( Eigen::MatrixXcd & z, Eigen::MatrixXcd & p, Eigen::MatrixXcd & k, double wo)
    {
        int degree;
        int result = _relative_degree(z,p,degree);

        if (result == 0)
        {
            int prev_row = z.rows();
            z = wo * z.inverse();

            z.conservativeResize(z.rows()+degree,1);            
            for (int i = prev_row; i < z.rows(); i++)
            {
                z(i,0) = 0.0;
            }

            p = wo * p.inverse();

            k = k*real(  (-z).prod() / (-p).prod()  );
        }

        return(result);       

    }    

    int _relative_degree(Eigen::MatrixXcd z, Eigen::MatrixXcd p, int & degree)
    {
        degree = p.rows() - z.rows();
        if (degree <0 )
        {
            std::cout << "Improper transfer function. Must have at least as many poles as zeros." << std::endl;
            return(2);
        }
        else
        {
            return(0);
        }
        
    }

    /**
     * @brief 
     * 
     * @param z 
     * @param p 
     * @param k 
     * @param b : Numerator
     * @param a : Denominator
     * @param ret_complexe_conj 
     * @return int 
     */
    int zpk2tf( Eigen::MatrixXcd z, Eigen::MatrixXcd p, Eigen::MatrixXcd k,Eigen::MatrixXcd & b, Eigen::MatrixXcd & a, bool & ret_complexe_conj)
    {
        if (z.cols() > 1)
        {
            std::cout << "Case z.cols()>1 (in zpk2tf) not implemented yet" << std::endl;
            return(3);
        }
        else
        {
            poly(z,b);
            //std::cout << "poly : " << b << std::endl;
            b = k(0,0)*b;
        }

        poly(p,a);
       
        // check if roots are reals
        bool complex_conj = true;
        int j = 0;

        while (j < z.cols() && complex_conj)
        {        
            std::vector<double> pos_root;
            std::vector<double> neg_root;
            for (int i = 0; i < z.rows(); i++)
            {
                double val = std::imag(z(i,j) );
                if (abs(val) > eps)
                {

                    if ( val > 0)
                    {
                        pos_root.push_back( val  );
                    }
                    else 
                    {
                        neg_root.push_back( -val  );
                    }
                }


            }
            std::sort( pos_root.begin(), pos_root.end());
            std::sort( neg_root.begin(), neg_root.end());

            complex_conj = ( ( pos_root.size() == 0 && neg_root.size() == 0 ) || (pos_root == neg_root ) );
            j++;
        }

        if (complex_conj)
        {
            for (int i = 0; i < a.rows(); i++)
            {
                for (int j = 0; j < a.cols(); j++)
                {
                    a(i,j) = std::real(a(i,j) );
                }
            }
        }
        else
        {
            ret_complexe_conj = complex_conj;
        }

        complex_conj = true;
        j = 0;

        while (j < p.cols() && complex_conj)
        {        
            std::vector<double> pos_root;
            std::vector<double> neg_root;
            for (int i = 0; i < p.rows(); i++)
            {
                double val = std::imag(p(i,j) );
                if (abs(val) > eps)
                {                
                    if ( val > 0)
                    {
                        pos_root.push_back( val  );
                    }
                    else 
                    {
                        neg_root.push_back( -val  );
                    }
                }
            }
            std::sort( pos_root.begin(), pos_root.end());
            std::sort( neg_root.begin(), neg_root.end());

            complex_conj = ( ( pos_root.size() == 0 && neg_root.size() == 0 ) || (pos_root == neg_root ) );
            j++;
        }

        if (complex_conj)
        {
            for (int i = 0; i < b.rows(); i++)
            {
                for (int j = 0; j < b.cols(); j++)
                {
                    b(i,j) = std::real(b(i,j) );
                }
            }
        }
        else
        {
            ret_complexe_conj = complex_conj;
        }


        return(0);

    }

    /**
     * @brief Inspired by https://stackoverflow.com/questions/33067755/how-to-efficiently-find-coefficients-of-a-polynomial-from-its-roots
     * Order : 
     * b[0]*x***n + b[1] * x**(n-1) + ... + b[N]
     * 
     * 
     * @param z 
     * @param b 
     */
    void poly(Eigen::MatrixXcd z, Eigen::MatrixXcd & b)
    {
        Eigen::MatrixXcd a(z.rows()+1, 1);
        // intialization : begin the polynome with the
        // first root. 
        a(1,0) = 1.0;
        a(0,0) = -z(0,0);
        for (int m = 2; m < a.rows(); m++)
        {

            std::complex<double> zm = z(m-1,0);
            a(m,0) = 1.0;
            // Eigen::MatrixXcd a_new = a;            
            // for (int k = m-1; k >= 1; k-- )
            // {
            //     a_new(k,0) = a(k-1,0) - a(k,0)*zm;
            // }
            // a = a_new;
            // a(m,0) = a(m-1,0);
            for (int k = m-1; k >= 1; k-- )
            {
                a(k,0) = a(k-1,0) - a(k,0)*zm;
            }
            a(0,0) = -a(0,0)*zm;
        }

        b.resize(z.rows()+1, 1);

        for (int i = 0; i < a.rows(); i++)
        {
            b(i, 0) = a( a.rows()-1 - i,0);
        }


    }




}