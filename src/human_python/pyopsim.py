#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 13:31:14 2020

@author: elandais
"""
import sys
import opensim as osim
import numpy as np
import os
from pathlib import Path


import algebra as alg
import random

import time

# method create TRC file
# nb : c3d2osim : https://github.com/dvarnai/pyC3D2OSIM

class OsimModel():
    """!
    Class creating a Osim Model, saving and updating useful informations about it.
    """
    def __init__(self,model_path,geometry_path,ik_path="",setup_ik_file="",trc_path="",visualize=False):
        """!
        
        Constructor of the OsimModel class.

        @param model_path : String.
            Absolute path to the .osim file (with the .osim filename).
            
        @param geometry_path : String.
            Path to the .vtp files associated with the .osim model.
            
        @param ik_path : String, optional.
            Path to the Inverse Kinematics configuration file. The default is "".
            
        @param setup_ik_file : String, optional.
            Name of the Inverse Kinematics configuration file. This file is used
            to set parameters of the Inverse Kinematics operation (weight of each
            markers of the model for example).
            The default is "".
            
        @param trc_path : String, optional.
            Path to a trc file associated with the .osim file. This file contains
            the different positions of each one of the markers of the model, at
            each time. This file is then used to determine the position of each
            one of the joints of the model at each time.
            The default is "".       
            
        @param visualize : Boolean, optional
            Activate the visualization of the model. The default is False.

        @return None.

        """
        if not Path(model_path).is_file():
            print("Error : " + model_path+ " not found.")
            exit(1)
        if not Path(geometry_path).is_file():
            print("Error : " + geometry_path+ " not found.")
            exit(1)
        

        
        ## Absolute path to the .osim file (with the .osim filename).
        self.model_path = model_path
        
        ## Absolute path to the Inverse Kinematics configuration file.
        self.compl_ik_path = ""
        
        ## Absolute path to the output motion file of the Inverse Kinematics.
        self.motion_file = ""
        
        if len(ik_path) != 0:
            try:
                os.mkdir(ik_path+"/output_IK/")
            except OSError:
                pass 
            if len(setup_ik_file)!=0:
                self.compl_ik_path = ik_path+"/"+setup_ik_file
            if len(trc_path)!=0:
                motion_name= trc_path.split("/")[-1].split(".")[0]
                self.motion_file = ik_path+"/output_IK/" + motion_name + ".mot"        
        
        ## Absolute path to the .trc file.
        self.trc_path= trc_path
        
        ## Absolute path to the .vtp files.
        self.geometry_path = geometry_path
        
        ## Object describing a .osim model, from the opensim module.
        self.osimModel = osim.Model(self.model_path)

        if visualize : 
            self.osimModel.setUseVisualizer(True)
            osim.ModelVisualizer.addDirToGeometrySearchPaths(geometry_path)
        
        ## Object describing the state of a .osim model, from the opensim module.
        self.osimState = self.osimModel.initSystem()
        
        ## Dictionary expressing, for each body part, its position and rotation matrix into the Ground referential.
        self.D_body_pos = {}
        
        ## Dictionary expressing, for each coordinate object, its value.
        self.D_joints_value = {}
        
        ## Dictionary expressing, for each joint, its position and rotation matrix in the global referential Ground.
        self.D_joints_pos = {}
        
        ## Dictionary expressing, for each joint, its position and rotation matrix in the referential of the parent joint.
        ## (Those values should be fixed)
        self.D_jp = {}    
        
        self.D_jp = getJointsPositions(self.osimModel,self.osimState,"Parent",n=0)     

        ## Dictionary linking the names of the coordinates objects to the name of their respective joints.
        self.D_coord_joints= {}
        
        ## Dictionary linking the names of the joints to the names of the coordinates objects associated with these joints. 
        self.D_joints_coord = {}
        
        self.D_coord_joints,self.D_joints_coord = getCorresJointsCoord(self.osimModel,self.osimState)           

        ## Dictionary linking the name of the coordinate objects with their minimum and maximum amplitude on their DOF.
        self.D_joints_range = getJointsLimits(self.osimModel,self.osimState)
        
        ## Dictionary linking the index associated with the value of the joints of the model, for the variable coordinateSet and the variable Q.
        self.D_Q_coord = getCorresQCoord(self.osimModel,self.osimState)
        
        ## Dictionary containing, for each of the coordinates (= DOF) names of the Ground joint, the type and the axis associated with this coordinate.
        self.D_gd_prop = getGroundProperties(self.osimModel)
        
        self.updModel()
        
    def updModel(self):
        """!
        
        Method used to update the different variables of the OsimModel class.
        This method should be called after each operation on the state of the
        .osim model.

        @return None.

        """
        self.D_joints_value = getJointsValues(self.osimModel,self.osimState)
        self.D_body_pos = getBodiesPositions(self.osimModel,self.osimState)
        self.D_joints_pos = getJointsPositions(self.osimModel,self.osimState,"Ground",n=1)        

    def runIK(self):
        """!
        
        Method used to execute the Inverse Kinematics operation. The variables 
        motion_file (=path to the output motion file of Inverse Kinematics), 
        compl_ik_path (= path to the Inverse Kinematics configuration file) and 
        trc_path (= path to the marker file used by Inverse Kinematics) should 
        be defined first. 
        Those variables are defined at the constructor step if the variables ik_path,
        setup_ik_file and trc_path were defined.

        @return None.

        """
        if len(self.motion_file) == 0 :
            print("Error : motion_file (=path to the output motion file of Inverse Kinematics) variable not defined")
        elif len(self.compl_ik_path) == 0 :
            print("Error : compl_ik_path (= path to the Inverse Kinematics configuration file) variable not defined")
        elif len(self.trc_path) == 0:
            print("Error : trc_path (= path to the marker file used by Inverse Kinematics) variable not defined")            
        else:
            ikFromFile(self.osimModel,self.osimState,self.trc_path,self.compl_ik_path,self.motion_file)
            self.updModel()
    
    def setQ(self,LQ,LQtype = 0):
        """!
        
        Set the Q vector of the .osim model, containing the values of each joint
        of this model. Those values are set in accordance to a list of variables
        LQ. 

        @param LQ : List.
        
            List containing the values of each joint of the model.
            
        @param LQtype : Int.
        
            Define the configuration of the LQ list.
            
            Type 0 : Configuration according to the order of the CoordinateSet
            variable of the .osim Model.
            LQ[i] corresponds to the desired value of CoordinateSet[i]. As there
            is no direct link between CoordinateSet[i] and Q[i], a transformation is
            necessary.
            
            Type 1 : Configuration according to the order of Q vector used by 
            osimState.
            LQ[i] corresponds to the desired value of Q[i] used by osimState. 
            Here, no transformation is necessary.            
            
            The default is 0.

        @return None.

        """
        
        RQ = []
        # set different configuration of the vector Q used for the .osim model.
        if LQtype ==0:
            for k in range(len(LQ)):
                RQ.append( LQ[self.D_Q_coord[k]] )
        elif LQtype == 1:
            RQ = LQ
        
        # set Q for the osim model
        setQ(self.osimModel,self.osimState,RQ)
        # update the variables of the OsimModel class.
        self.updModel()
         

        
  
def getGroundProperties(osimModel):
    """!
    
    Method used to gather some useful properties about the Ground link
    of the .osim model. 

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @return D_gd_prop : Dictionary of list.
        Dictionary containing, for each of the coordinates (= DOF) names of the 
        Ground joint, the type and the axis associated with this coordinate.

    """
    jointlist = osimModel.getJointList()
    gd_j = None
    D_gd_prop = {"joint_name" : "",
                "joint_type": "",
                 "properties" : {}}
    
    for joint in jointlist:
        p_frame = joint.get_frames(0)
        if type(gd_j)==type(None) and "ground" in p_frame.getName():
            gd_j = joint

    if gd_j != None:
        D_gd_prop["joint_name"] = gd_j.getName()
        
        type_joint = str( type(gd_j) )[:-2].split(".")[-1]
                
        D_gd_prop["joint_type"] = type_joint
        if type_joint == "CustomJoint":

            trans = gd_j.getSpatialTransform()
            
            rot1 = trans.get_rotation1()
            rot2 = trans.get_rotation2()
            rot3 = trans.get_rotation3()
            
            tr1 = trans.get_translation1()
            tr2 = trans.get_translation2()
            tr3 = trans.get_translation3()
            
            L_trans = [rot1,rot2,rot3,tr1,tr2,tr3]
            L_trans_name = ["rotation1",
                            "rotation2",
                            "rotation3",
                            "translation1",
                            "translation2",
                            "translation3"]
            
            axes = [  numpy(x.getAxis() ) for x in L_trans ]
            names_axes = [ numpy(x.getCoordinateNamesInArray()) for x in L_trans]
    
            for typ,axe,name in zip(L_trans_name,axes,names_axes):
    
                D_gd_prop["properties"][name[0,0]] = [typ,axe]
    return(D_gd_prop)

    
    
def getCorresQCoord(osimModel,osimState):
    """!
    
    Method used to get the correspondance between the index associated with
    the value of the joints of the model, for the variable coordinateSet 
    and the variable Q.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return D_Q_coord : Dictionary.   
        D_Q_coord is organised as follows : 
            D_Q_coord[i] = j <--->  Q[i] = coordinateSet[j].getValue(osimState).
        
        The value of coordinate j (wrt CoordinateSet) corresponds to the i-th component
        of Q from osimState.

    """
    coordinateSetlist = osimModel.getCoordinateSet()
     
    D_Q_coord = {}
     
    NQ = osimState.getNQ()
     
    LQ = [0. for k in range(NQ)]    
    for i in range(NQ):

        LQ[i] = 0.25
        setQ(osimModel,osimState,LQ)
        j  = 0
        for coord in coordinateSetlist:
            val = coord.getValue(osimState)
            if val == 0.25:
                break
            else:
                j+=1
        D_Q_coord[i] = j
        LQ[i] = 0.
    return(D_Q_coord)
   


def printCorresQCoord(osimModel,osimState):
    """!
    
    Method used to print the correspondance between the index associated with
    the value of the joints of the model, for the variable coordinateSet 
    and the variable Q. This method is based on the method getCorresQCoord.
    
    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return None.

    """
    D_Q_coord = getCorresQCoord(osimModel,osimState)
    
    coordinateSetlist = osimModel.getCoordinateSet()

    l=0
    for coord in coordinateSetlist:
        name = coord.getName()
        print("Name : {:<24} : index in coordinateSetlist : {: d} | Q : {:d}"
              .format(name, l, D_Q_coord[l]))
        l+=1
    

def getJointsLimits(osimModel,osimState):
    """!
    
    Method used to get the limits values of each one of the DOF of the model.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return D_joints_range : Dictionary of List
        D_joints_range is organised as follows : 
            D_joints_range[i] = [lower limit of DOF i, upper limit of DOF i].

    """
    coordinateSetlist = osimModel.getCoordinateSet()
    D_joints_range = {}  

    for coord in coordinateSetlist:
       name = coord.getName()
       lim = [coord.get_range(0),coord.get_range(1) ]
       D_joints_range[name] = lim
    return(D_joints_range)

def printJointsLimits(osimModel,osimState):
    """!
    
    Method used to print the limits values of each one of the DOF of the model,
    based on the method getJointsLimits

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return None.

    """
    D_joints_range = getJointsLimits(osimModel,osimState)
    
    print("--- Limits (lower upper) of each articulation ---")
    for name in D_joints_range:
       lim = D_joints_range[name]
       print("Name {:<24} : {: .4f} |  {: .4f}".format(name,lim[0], lim[1]))


def getCorresJointsCoord(osimModel,osimState):
    """!
    
    Method used to get a quick access to the correspondances between the joints
    of the model and the coordinates associated with each one of those joints.
    
    A coordinate is associated to an unique joint, but a joint can have multiple
    coordinates. 

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return D_coord_joints : Dictionary.
        Dictionary linking the names of the coordinates objects to the name of
        their respective joints.
        Ex : D_coord_joints[pelvis_tx] = "pelvis"
            D_coord_joints[pelvis_ty] = "pelvis"
            D_coord_joints[pelvis_tz] = "pelvis"
            D_coord_joints[pelvis_rx] = "pelvis"
            D_coord_joints[pelvis_ry] = "pelvis"
            D_coord_joints[pelvis_rz] = "pelvis"
 
    
    @return D_joints_coord : Dictionary. 
        Dictionary linking the names of the joints to the names of the coordinates
        objects associated with these joints. 
        Ex : D_joints_coord[pelvis] = [pelvis_tx,pelvis_ty,pelvis_tz,pelvis_rx,pelvis_ry,pelvis_rz]
 
    """
    coordinateSetlist = osimModel.getCoordinateSet()

    D_joints_coord = {}
    D_coord_joints = {}

    for coord in coordinateSetlist:
        coord_name = coord.getName()
        coord_joint = coord.getJoint()
        
        joint_name = coord_joint.getName()

        if joint_name not in D_joints_coord:
            D_joints_coord[joint_name] = [coord_name]
        else:
            D_joints_coord[joint_name].append(coord_name)
        D_coord_joints[coord_name] = joint_name
    
    # Some special joints may not have any coordinates (ex : ground)
    # This is used to deal with this special case. 
    jointlist = osimModel.getJointList()
    for joint in jointlist:
        joint_name = joint.getName()
        if joint_name not in D_joints_coord:
            D_joints_coord[joint_name] = [joint_name]
            D_coord_joints[joint_name] = joint_name

    return(D_coord_joints,D_joints_coord)
    

def printJointsDivisionsIntoOneDOF(osimModel,osimState):
    """!
    
    Method used to print the correspondances between the joints
    of the model and the coordinates associated with each one of those joints.
    Based on the method getCorresJointsCoord.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return None.

    """
    print("--- Division of each joints into one DOF  ---")
    
    D_coord_joints,D_joints_coord = getCorresJointsCoord(osimModel, osimState)
    
    for joint_name in D_joints_coord:
        print("Name of the joint : %s ; elements division : "%joint_name, D_joints_coord[joint_name])
        

def getJointsPositions(osimModel,osimState,refe="Ground",n=1):
    """!
    Method used to get the positions of one of the two frames of the joints
    of the model. The positions of the selected frames can then be expressed into 
    the global referential "Ground" of the model, or the referential of the parent
    joint "Parent" of the selected joint.
    
    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @param refe : String, optional.
    
        Type of referential chosen to express the joints position. Two choices
        are available : 
        
        refe = "Ground", to express the positions into the global referential 
        Ground. 
        
        refe = "Parent", to express the positions into the referential of the parent
        joint of the selected joint. This could be useful to determine the position 
        of the joint compared to the previous joint.
            
        The default is "Ground".
    
    @param n : Int, optional.
    
        Value associated with the joint frame to be selected for each of the 
        joints.
        Two frames are available : one frame fixed in relation to the parent joint, 
        the other fixed in relation to the current joint. 
        
        n = 0 : frame fixed in relation to the parent joint.
        
        n = 1 : frame fixed in relation to the current joint.
    
        The default is 1.

    @return D_joints_pos : Dictionary of list.
        Dictionary expressing, for each joint, its position and rotation matrix 
        in the chosen referential. 
        
        Example : 
            D_joints_pos[pelvis] = [ M_p, M_R ]
            M_p = np.array([[0.],
                            [2.],
                            [0.]])
            M_R = np.eye(3,3)

    """
    jointlist = osimModel.getJointList()
    D_joints_pos = {}
    for joint in jointlist:
       name = joint.getName()
       p_frame = joint.get_frames(n)
       if refe == "Ground":
           p = p_frame.getTransformInGround(osimState).p()
           R = p_frame.getTransformInGround(osimState).R().asMat33()
       elif refe == "Parent":
           pp_frame = p_frame.getParentFrame()
           transf = p_frame.findTransformBetween(osimState,pp_frame)
           p = transf.p()
           R = transf.R().asMat33()
       M_R = numpy(R)      
       M_p = numpy(p)
       D_joints_pos[name] = [M_p,M_R]
    return(D_joints_pos)
       

def displayModel(osimModel,osimState,trc_path,compl_ik_path):
    """!

    (BETA). Method used to display a .osim Model.            

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.
        
    @param trc_path : String
            Path to a trc file associated with the .osim file. This file contains
            the different positions of each one of the markers of the model, at
            each time. This file is then used to determine the position of each
            one of the joints of the model at each time.

            
    @param compl_ik_path : String.
            Complete path to the Inverse Kinematics configuration file. 

    @return None.

    """
    toolIK = osim.InverseKinematicsTool(compl_ik_path)
    toolIK.set_marker_file(trc_path)
    toolIK.setModel(osimModel)

    manager = osim.Manager(osimModel)
    manager.initialize(osimState)
    manager.integrate(4.0)    


def printJointsPositionsAllFrames(osimModel,osimState,refe = "Ground"):
    """!
    
    Method used to print the positions of all the frames of each joint, into a 
    chosen referential "refe".

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.
        
    @param refe : String, optional.
    
        Type of referential chosen to express the joints position. Two choices
        are available : 
        
        refe = "Ground", to express the positions into the global referential 
        Ground.
        
        refe = "Parent", to express the positions into the referential of the parent
        joint of the selected joint. This could be useful to determine the position 
        of the joint compared to the previous joint.
            
        The default is "Ground".

    @return None.

    """
    print("--- Position of each joint referential in %s referential  ---"%refe)
    jointlist = osimModel.getJointList()
    for joint in jointlist:
       name = joint.getName()
       p_frame = joint.get_frames(0)
       if refe == "Ground":       
           p = p_frame.getTransformInGround(osimState).p()
           Q = p_frame.getTransformInGround(osimState).R().convertRotationToQuaternion()
       elif refe == "Parent":
           pp_frame = p_frame.getParentFrame()
           transf = p_frame.findTransformBetween(osimState,pp_frame)
           p = transf.p()
           Q = transf.R().convertRotationToQuaternion()                    
       M_q = numpy(Q)       
       r,pc,y = alg.euler_from_quaternion(M_q.flatten(),axes = 'sxyz')
       print("Name %s"%name)
       print("Parent Frame : {: .4f} {: .4f} {: .4f} | {: .4f} {: .4f} {: .4f} ".format(p.get(0), p.get(1), p.get(2), r,pc,y ) )
       c_frame = joint.get_frames(1)
       if refe == "Ground":       
           p = c_frame.getTransformInGround(osimState).p()
           Q = c_frame.getTransformInGround(osimState).R().convertRotationToQuaternion()
       elif refe == "Parent":
           pp_frame = c_frame.getParentFrame()
           transf = c_frame.findTransformBetween(osimState,pp_frame)
           p = transf.p()
           Q = transf.R().convertRotationToQuaternion()                    
       M_q = numpy(Q)       
       r,pc,y = alg.euler_from_quaternion(M_q.flatten(),axes = 'sxyz')       
       print("Child Frame  : {: .4f} {: .4f} {: .4f} | {: .4f} {: .4f} {: .4f} ".format(p.get(0), p.get(1), p.get(2), r,pc,y ) )

def printJointsPositions(osimModel,osimState,refe="Ground", n= 1):
    """!
    Method used to print the positions of one of the two frames of the joints
    of the model, into a chosen referential. Based on the method getJointsPositions.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @param refe : String, optional.
    
        Type of referential chosen to express the joints position. Two choices
        are available : 
        
        refe = "Ground", to express the positions into the global referential 
        Ground. 
        
        refe = "Parent", to express the positions into the referential of the parent
        joint of the selected joint. This could be useful to determine the position 
        of the joint compared to the previous joint.
            
        The default is "Ground".
    
    @param n : Int, optional.
    
        Value associated with the joint frame to be selected for each of the 
        joints.
        Two frames are available : one frame fixed in relation to the parent joint, 
        the other fixed in relation to the current joint. 
        
        n = 0 : frame fixed in relation to the parent joint.
        
        n = 1 : frame fixed in relation to the current joint.
    
        The default is 1.

    @return None.

    """
    
    
    print("--- Position of each joint referential in %s referential  ---"%refe)
    jointlist = osimModel.getJointList()
    for joint in jointlist:
       name = joint.getName()
       p_frame = joint.get_frames(n)
       if refe == "Ground":
           p = p_frame.getTransformInGround(osimState).p()
           Q = p_frame.getTransformInGround(osimState).R().convertRotationToQuaternion()
       elif refe == "Parent":
           pp_frame = p_frame.getParentFrame()
           transf = p_frame.findTransformBetween(osimState,pp_frame)
           p = transf.p()
           Q = transf.R().convertRotationToQuaternion()           
       M_q = numpy(Q)       
       r,pc,y = alg.euler_from_quaternion(M_q.flatten(),axes = 'sxyz')
       print("Name {:<24} : {: .4f} {: .4f} {: .4f} | {: .4f} {: .4f} {: .4f} ".format(name, p.get(0), p.get(1), p.get(2), r,pc,y ) )


def printBodiesPositions(osimModel,osimState):
    """!
    Method used to get the positions of the body parts of the model into the
    ground referential.
    

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.
    
    @return None.

    """
    
    print("--- Position of each referential associated with each body part in Ground referential  ---")
    bodylist = osimModel.getBodyList()
    for body in bodylist:
       name = body.getName()
       p = body.getTransformInGround(osimState).p()
       str_R = body.getTransformInGround(osimState).R().asMat33().toString()
       print("Name {:<24} : {: .4f} {: .4f} {: .4f} | ".format(name, p.get(0), p.get(1), p.get(2)), str_R)

def numpy(matrix):
    """!
    
    Method used to transform OpenSim Mat33 or Vec into numpy objects.

    @param matrix : opensim.simbody.Mat33 or opensim.simbody.Vec.
        Input Opensim matrix.

    @return M : numpy.array.
        Output matrix, as array.

    """
    mat = True
    mat_type = None
    
    try:
        matrix.get(0,0)
    except Exception:
        mat = False

    if mat:
        mat_type = type(matrix.get(0,0))
        M = Mnumpy(matrix,mat_type)
    else:
        mat_type = type(matrix.get(0))
        M = Vnumpy(matrix,mat_type)
    return(M)

def Vnumpy(matrix,mat_type):
    """!
    
    Transform a Vec object into a numpy array.

    @param matrix : opensim.simbody.Vec.
        Opensim Vector
    @param mat_type : type.
        Type of the vector (String, float, int, ...)

    @return V : numpy.array.
        Vector array.

    """
    nrow = matrix.size()
    V= np.zeros((nrow,1)).astype(mat_type)
    for i in range(nrow):
        V[i,0] = matrix.get(i)
    return(V)

def Mnumpy(matrix,mat_type):
    """!
    
    Transform a Mat33 object into a numpy array.

    @param matrix : opensim.simbody.Mat33.
        Opensim Mat33
    @param mat_type : type.
        Type of the vector (String, float, int, ...)

    @return M : numpy.array.
        Matrix array.

    """    
    ncol = matrix.ncol()
    nrow = matrix.nrow()
    M = np.zeros((nrow,ncol)).astype(mat_type)
    
    for i in range(nrow):
        for j in range(ncol):
            M[i,j] = matrix.get(i,j)
    return(M)

def getBodiesPositions(osimModel,osimState):
    """!
    
    Method used to get the positions of the body parts of the model, expressed
    into the global referential Ground.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return D_body_pos : Dictionary of list.
    Dictionary expressing, for each body part, its position and rotation matrix
    into the Ground referential.
    
    Example : 
            D_body_pos[hand_l] = [ M_p, M_R ]
            M_p = np.array([[0.5],
                            [0.5],
                            [0.75]])
            M_R = np.eye(3,3)

    """
    
    bodylist = osimModel.getBodyList()
    
    D_body_pos = {}
    
    for body in bodylist:
       name = body.getName()
       M_R = np.zeros((3,3))
       M_p = np.zeros((3,1))
       p = body.getTransformInGround(osimState).p()
       R = body.getTransformInGround(osimState).R().asMat33()
       M_R = numpy(R)
       M_p = numpy(p)

       D_body_pos[name] = [M_R,M_p]
    return(D_body_pos)
            

def getJointsValues(osimModel,osimState):
    """!
    
    Method used to get, for each coordinate object of the .osim model, its value.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return D_joint_value : Dictionary.
    Dictionary expressing, for each coordinate object, its value.
    Example : 
            D_joint_value[pelvis_ty] = 2.0

    """
    coordinateSetlist = osimModel.getCoordinateSet()
    D_joint_value = {}    

    for coord in coordinateSetlist:
       name = coord.getName()
       D_joint_value[name] = coord.getValue(osimState)
    return(D_joint_value)


def printFramesNumber(osimModel):
    """!
    
    Method used to print the index and the name of each frame associated 
    with the model.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @return None.

    """
    print("--- Number associated with each frame ---")
    framelist = osimModel.getFrameList()
    n = 0
    for frame in framelist:
       name = frame.getName()
       
       print("Name {:<24} : {: .4f}  ".format(name,n) )
       n+=1
    
 
def printJointsValues(osimModel,osimState):
    """!
    
    Method used to print the value of each of the coordinates of the model.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return None.

    """
    coordinateSetlist = osimModel.getCoordinateSet()
    print("--- Value of each articulation ---")
    for coord in coordinateSetlist:
       name = coord.getName()
       coordval = coord.getValue(osimState)
       print("Name {:<24} : {: .4f} ".format(name,coordval))

def getMarkersByBodyParts(osimModel):
    markerSet = osimModel.upd_MarkerSet()
    D_body_markers = {}
    for marker in markerSet:
        name = marker.getName() 
        phys_name = marker.getParentFrame().getName()
        frame_name = phys_name.split("/")[-1]
        if frame_name not in D_body_markers:
            D_body_markers[frame_name] = []
        D_body_markers[frame_name].append(name)


    # for i in range(markerSet.getSize()):
    #     name =osimModel.get_MarkerSet(i).getName()
    #     phys_name = osimModel.get_MarkerSet(i).getParentFrame().getName()
    #     frame_name = phys_name.split("/")[-1]
    #     if frame_name not in D_body_markers:
    #         D_body_markers[frame_name] = []

    return(D_body_markers)

def getBodiesDistancesParentChild(osimModel,osimState):
    """
        body : [ [index_parent, dist], [ [index_child_i, dist_i] ]  ]
    """
    
    bodylist = osimModel.getBodyList()
    
    jointlist = osimModel.getJointList()

    D_body_parent_child_distances = {}
    
    D_body_index = {}
    
    i = 0
    for body in bodylist:
       name = body.getName()
       
       D_body_parent_child_distances[name] = [ [], [] ]
       
       D_body_index[name] = i
       i+=1
      
    for joint in jointlist:

       
       M_R = np.zeros((3,3))
       M_p = np.zeros((3,1))
       
       # let's try to get informations about the frame 0
       
       frame_cons = joint.get_frames(0)
          
       p_frame = frame_cons.getParentFrame()
       
       # pp_frame = p_frame.getParentFrame()
       
       p_name = p_frame.getName()

       
       p_cons_transf = frame_cons.findTransformBetween(osimState,p_frame)
       
       p = p_cons_transf.p()
       R = p_cons_transf.R().asMat33()
       M_R = numpy(R)
       M_p = numpy(p)       
       p_dist = np.linalg.norm( np.dot(M_R,M_p) )
       
       try :
           p_index = D_body_index[p_name]
       except KeyError:
           p_index = -1
       
       frame_cons2 = joint.get_frames(1)
       c_frame = frame_cons2.getParentFrame()
       #c_frame = frame_cons2
       c_name = c_frame.getName()

       c_cons_transf = frame_cons2.findTransformBetween(osimState,c_frame)
       
       p = c_cons_transf.p()
       R = c_cons_transf.R().asMat33()
       M_R = numpy(R)
       M_p = numpy(p)       
       c_dist = np.linalg.norm( np.dot(M_R,M_p) )

       try :
           c_index = D_body_index[c_name]
       except KeyError:
           c_index = -1
       
       try :
           D_body_parent_child_distances[c_name][0] = [p_index,p_dist]
       except KeyError:
           pass
       try:
           D_body_parent_child_distances[p_name][1].append( [c_index,p_dist] )
       except KeyError:
           pass

    return(D_body_index,D_body_parent_child_distances)
                


def fixScalingFactors(osimModel, osimState, attribuedBodies):
    
    # nom joint -> pos par rapport au parent.
    D_jp = getJointsPositions(osimModel,osimState,"Parent",n=0)
    
    D_body_index,D_body_parent_child_distances = getBodiesDistancesParentChild(osimModel,osimState)
    
    bodylist = osimModel.getBodyList()
    
    i = 0
    for body in bodylist:
        body_name = body.getName()
        
        if body_name not in attribuedBodies and body_name!="ground":
            L_concerned = D_body_parent_child_distances[body_name]
            
            min_dist = 9999
            min_index = 0
            pair_parent = L_concerned[0]
            
            if pair_parent[0] != -1:
                min_dist = pair_parent[1]
                min_index = pair_parent[0]
            
            for k in range(len(L_concerned[1])):
                pair_child = L_concerned[1][k]
                
                if len(pair_child) >0 and pair_child[0] !=-1:
                    if min_dist > pair_child[1]:
                        min_dist = pair_child[1]
                        min_index = pair_child[0]
            body_chosen = body
            bodySet = osimModel.getBodySet()

            body_chosen = bodySet.get(min_index)
            
            scaleFactors = body_chosen.get_attached_geometry(0).get_scale_factors()
            print("old scale : ", osimModel.updBodySet().get(i).get_attached_geometry(0).get_scale_factors())
            
            osimModel.updBodySet().get(i).scale(scaleFactors)
            
            print("New scale factors : ", osimModel.updBodySet().get(i).get_attached_geometry(0).get_scale_factors())
            
                                     
        i+=1
    return(osimModel,osimState)

def reassembleByAxis(osimModel, osimState, D_body_markers, parallal_val = 0.8):
    """

    For each body part, create 


    """

    D_mark_pos = getMarkersPositionsInLocalReferential(osimModel)
    #print(D_mark_pos)

    D_body_markerPair = {}

    bodyNames = []
    for body in D_body_markers:

        bodyNames.append(body)        
        list_mark = D_body_markers[body]
        
        L_body_marker_vector = []

        L_body_marker_pair = []

        for i in range(len(list_mark)):
            
            mark_i = list_mark[i]

            pos_i = D_mark_pos[mark_i]

            
            for j in range(i+1, len(list_mark)):

                mark_j = list_mark[j]
                
                pos_j = D_mark_pos[mark_j]

                n_vec = (pos_j-pos_i)/np.linalg.norm(pos_j-pos_i)

                L_body_marker_vector.append(n_vec)

                L_body_marker_pair.append([mark_i,list_mark[j]])
        
        # search composantes on X Y Z
        D_body_markerPair[body] = {'X':[], 'Y':[], 'Z':[] }
        #D_index = {'X':[], 'Y':[], 'Z':[]}
        L = ['X','Y','Z']

        L_max_vec = [L_body_marker_vector[0], L_body_marker_vector[0], L_body_marker_vector[0]]

        L_index = [0,0,0]

        #print("For body : ", body)

        for i in range(len(L_body_marker_vector) ):

            #print("For pair : ", L_body_marker_pair[i])

            vec = L_body_marker_vector[i]

            #print("Vector : ", vec)
                        
            for k in range(3):
                compo = abs(vec[k])

                if compo > parallal_val :
                    D_body_markerPair[body][L[k]].append(L_body_marker_pair[i])


                if compo > abs(L_max_vec[k][k]):
                    L_max_vec[k] = vec
                    L_index[k] = i
        for k in range(3):
            if len(D_body_markerPair[body][L[k]]) == 0:
                D_body_markerPair[body][L[k]].append(L_body_marker_pair[L_index[k]])
        
        #print("----------")

    return(D_body_markerPair,bodyNames)


def scaling(osimModel,osimState, dirPath, osimFile, scaleFile, trcFile, motFile, mass, scalingFactors):
    """
    Inspired from https://github.com/HernandezVincent/OpenSim
    
    """
    
    
    LosimPath = osimFile.split("/")
    
    osimPath = LosimPath[0]
    for i in range(1,len(LosimPath)-1):
        osimPath+= "/"+ LosimPath[i]
    
    osimPath+="/"
    
    model_name = LosimPath[-1].split(".")[0]
    D_body_markers = getMarkersByBodyParts(osimModel)
    
    
    generic_MM_Path = osimFile                # Path to the Generic Musculoskeletal Model
    XML_generic_ST_path = scaleFile                            # Path to the generic Scale Tool XML file
    TRC_file = trcFile                                            # Path to the .trc file that contain the marker data
    
    XML_ST_file = dirPath + 'scaleToolSubject.xml'                             # Path to the subject Scale Tool XML file that will be created
    XML_SF_file = dirPath + 'scaleFactorSubject.xml'                           # Path to the subject Scale Factor file that will be created
    scaled_MM_path = dirPath + model_name+ 'Scaled.osim'                        # Path to the subject Musculoskeletal Model that will be created with the model scaler
    scaled_MM_path2 = osimPath + model_name + 'ScaledPlaced.osim'                  # Path to the subject Musculoskeletal Model that will be created with the marker placer

    markerData = osim.MarkerData(TRC_file)
    initial_time = markerData.getStartFrameTime()
    final_time = markerData.getLastFrameTime()
    TimeArray = osim.ArrayDouble()                                                 # Time range
    TimeArray.set(0,initial_time)
    TimeArray.set(1,final_time) 
    
    # Scale Tool
    scaleTool = osim.ScaleTool(XML_generic_ST_path)
    scaleTool.setName(model_name)                                                   # Name of the subject
    scaleTool.setSubjectMass(mass)                                                   # Mass of the subject
    scaleTool.setSubjectHeight(-1)                                                 # Only for information (not used by scaling)
    scaleTool.setSubjectAge(-1)                                                    # Only for information (not used by scaling)
    
    # Generic Model Maker
    scaleTool.getGenericModelMaker().setModelFileName(osimFile)
    #scaleTool.getGenericModelMaker().setMarkerSetFileName(XML_markers_path)
    
    # Model Scaler
    scaleTool.getModelScaler().setApply(True)

    scaleTool.getModelScaler().setMarkerFileName(TRC_file)                          
    scaleTool.getModelScaler().setTimeRange(TimeArray)
    scaleTool.getModelScaler().setPreserveMassDist(True)
    scaleTool.getModelScaler().setOutputModelFileName(scaled_MM_path)
    scaleTool.getModelScaler().setOutputScaleFileName(XML_SF_file)
    scaleTool.getModelScaler().setPreserveMassDist(True)
    
    # The scale factor information concern the pair of marker that will be used
    # to scale each body in your model to make it more specific to your subject.
    # The scale factor are computed with the distance the virtual markers and between your experimental markers
    

    
    
    measurementTemp = osim.Measurement()
    bodyScaleTemp = osim.BodyScale()
    markerPairTemp = osim.MarkerPair()
    scaleTemp = osim.Scale()
    
    bodyNames = []
    
    markerList = []

    for body in D_body_markers:
        markerList += D_body_markers[body]    

    
    isScalingFixed = ( len(scalingFactors) == len(D_body_markers) )
    
    print("Scaling is Fixed by user : ", isScalingFixed)
    
    if not isScalingFixed:
        scaleTool.getModelScaler().setScalingOrder(osim.ArrayStr('measurements', 1))        
        
        D_body_markerPair, bodyNames = reassembleByAxis(osimModel,osimState,D_body_markers)
        #print("Pairs created : ", markerPairList)
        nBody = len(bodyNames)
    
        # Create a Marker Pair Set fo each body


        for i in range(0, nBody):
    
            print("For body : ", bodyNames[i])
            
            L = ['X','Y','Z']
            for k in range(3):
                
                measurement = measurementTemp.clone()
                measurement.setName(bodyNames[i]+ '_'+ L[k]) # Whatever name you want(Usually I set the same name as the body)
                # Create a Body Scale Set
                bodyScale = bodyScaleTemp.clone()
                bodyScale.setName(bodyNames[i]) # Name of the body
                bodyScale.setAxisNames(osim.ArrayStr(L[k], 1))  # instead of 'X Y Z'
    
                # Add body Scale set to Scaling
                measurement.setApply(True)
                measurement.getBodyScaleSet().adoptAndAppend(bodyScale)   
    
                markerPairList = D_body_markerPair[bodyNames[i]][L[k]] 
    
                print(" Axis %s : "%L[k], markerPairList)
    
                for j in range(len(markerPairList)):
    
                    # Create a Marker Pair Set
                    markerPair = markerPairTemp.clone()
                    markerPair.setMarkerName(0, markerPairList[j][0])
                    markerPair.setMarkerName(1, markerPairList[j][1])
    
                    # Update the measurement to add the marker Pair
            
                    measurement.getMarkerPairSet().adoptAndAppend(markerPair)
    
                    # Add the measurement to the Model Scaler
                scaleTool.getModelScaler().addMeasurement(measurement)
                
    else:
        
        for body in D_body_markers:
            bodyNames.append(body)
        
        scaleTool.getModelScaler().setScalingOrder(osim.ArrayStr('manualScale', 1))

        for body in D_body_markers:
            
            scale = scaleTemp.clone()
            
            scale.setSegmentName(body)
            
            arD = osim.ArrayDouble(0.0,3)
            

            sc = scale.getScaleFactors()
            
            for i in range(3):
                sc.set(i,scalingFactors[body][i])
            
            scale.setScaleFactors(sc)
            scale.setApply(True)
            
            scaleTool.getModelScaler().addScale(scale)

        
    # Create the subject Scale Tool XML file
    scaleTool.printToXML(XML_ST_file)
    print('Scale step : XML files : ' +  XML_ST_file + ' created')
    
    # Launch the scale tool again with the new XML file and then scale the
    # generic musculoskeletal model
    scaleTool = osim.ScaleTool(XML_ST_file)
    
    # Scale the model
    scaleTool.getModelScaler().processModel(osimModel)
    print('Scaled Musculoskeletal : ' + scaled_MM_path + ' created')
    
    # In this part, we will use the previous XML file created and update it
    # with the MarkerPlacer tool to Adjust the position of the marker on the
    # scaled musculoskeletal model
    
    # Load the scaled musculoskeletal model
    osimModelScaled = osim.Model(scaled_MM_path)
    osimStateScaled = osimModelScaled.initSystem()
    
    # Added : first, fix the scale factors that might be wrong
    # (ex : no markers on them)
    osimModelScaled,osimStateScaled = fixScalingFactors(osimModelScaled,osimStateScaled,bodyNames)
    
    # Launch the scale tool
    scaleTool = osim.ScaleTool(XML_ST_file)
    
    # Get the marker data from a.trc file
    # markerData = osim.MarkerData(TRC_file)
    # initial_time = markerData.getStartFrameTime()
    # final_time = markerData.getLastFrameTime()
    # TimeArray = osim.ArrayDouble() # Time range
    # TimeArray.set(0, initial_time)
    # TimeArray.set(1, final_time)
    
    # The static pose weights will be used to adjust the markers position in 
    # the model from a static pose. The weights of the markers depend of the
    # confidence you have on its position.In this example, all marker weight
    # are fixed to one.
    
    scaleTool.getMarkerPlacer().setApply(True) # Ajustement placement de marqueurs(true or false)
    scaleTool.getMarkerPlacer().setStaticPoseFileName(TRC_file) # trc files for adjustements(usually the same as static)
    scaleTool.getMarkerPlacer().setTimeRange(TimeArray) # Time range
    #scaleTool.getMarkerPlacer().setCoordinateFileName(motFile)
    scaleTool.getMarkerPlacer().setOutputModelFileName(scaled_MM_path2)
    scaleTool.getMarkerPlacer().setMaxMarkerMovement(0.0)
    
    measurementTemp = osim.Measurement()
    ikMarkerTaskTemp = osim.IKMarkerTask()
    
    for i in range(0, len(markerList) ):
    
        ikMarkerTask = ikMarkerTaskTemp.clone()
    
        ikMarkerTask.setName(markerList[i]) # Name of the markers
        ikMarkerTask.setApply(True)
        ikMarkerTask.setWeight(1.0)
    
        scaleTool.getMarkerPlacer().getIKTaskSet().adoptAndAppend(ikMarkerTask)
    
    # Create the subject Scale Tool XML file
    scaleTool.printToXML(XML_ST_file)
    print('Placer step : XML files : ' + XML_ST_file + ' created')
    
    # Launch the ScaleTool again
    scaleTool = osim.ScaleTool(XML_ST_file)
    scaleTool.getMarkerPlacer().processModel(osimModelScaled) #processModel(osimStateScaled, osimModelScaled)
    print('Adjusted markers on the musculoskeletal done')
    
    

                
                
        
        
    
    

    
def ikFromFile(osimModel,osimState,trc_path,compl_ik_path,mot_path):
    """!

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.


    @param trc_path : String
            Path to a trc file associated with the .osim file. This file contains
            the different positions of each one of the markers of the model, at
            each time. This file is then used to determine the position of each
            one of the joints of the model at each time.

            
    @param compl_ik_path : String.
            Complete path to the Inverse Kinematics configuration file. 

    @param mot_path : String
        Path to the output motion file of Inverse Kinematics.

    @return None.

    """
    path, filename = os.path.split(trc_path)
    filename, ext = os.path.splitext(filename)

    # Marker Data

    markerData = osim.MarkerData(trc_path)

    # Set the IK tool
    ikTool = osim.InverseKinematicsTool(compl_ik_path)
    ikTool.setModel(osimModel)
    ikTool.setName(filename + ext)
    ikTool.setMarkerDataFileName(trc_path)


    ikTool.setOutputMotionFileName(mot_path)

    ikTool.run()
    

def IK_from_markers_positions(osimModel,osimState,markers_positions,xml_taskset,trc_path):
    
    # voir IK from file de pyosim
    s = osimModel.initSystem()
    markers_ref = osim.MarkersReference()
    
    aweights = markers_ref.getMarkerWeightSet()
    
    
    ikTool = osim.InverseKinematicsTool(xml_taskset)
    
    IKTaskSet = ikTool.getIKTaskSet()
    
    #IKTaskSet = osim.IKTaskSet(xml_taskset)
    #print(IKTaskSet.getWeight())
    
    IKTaskSet.createMarkerWeightSet(aweights)
    
    print("size : " ,aweights.getSize())
    print("num : " ,aweights.get(0).getWeight())
    
    markerNames = []
    
    L = osim.ArrayStr()
    aweights.getNames(L)
    
    
    
    for i in range(L.getSize()):
        markerNames.append(L.get(i))
    
    
    M = osim.MatrixVec3(1,len(markerNames))
    for i in range(len(markerNames)):
        V = osim.Vec3()
    
        V.set(0,markers_positions[3*i])
        V.set(1,markers_positions[3*i+1])
        V.set(2,markers_positions[3*i+2])
    
        M.set(0,i,V)
        
    
    #table2 = osim.TimeSeriesTable([0.0],M,label_col)


    table = osim.TimeSeriesTableVec3([0.0],M,markerNames)
    
    # print(table)
    # print(type(table))
    
    nmarker_ref = osim.MarkersReference(table,aweights)
    
    
    coord_ref = osim.SimTKArrayCoordinateReference(len(markerNames))
    
    #coord_ref.getConcreteClassName()
    obj = nmarker_ref
    
    dwn = nmarker_ref.safeDownCast(obj)
    print(nmarker_ref)
    
    print(dwn)
    
    #IKSolver = osim.InverseKinematicsSolver(osimModel,nmarker_ref.safeDownCast(obj),coord_ref)
    
    #markers_ref.setMarkerWeightSet(aweights)
    
    
    
    # tst_m_ref = osim.MarkersReference(trc_path,aweights)
    
    # table = tst_m_ref.getMarkerTable()
    # #print(table)
    
    # #M = osim.simTK
    # M = osim.Matrix(1,2)
    # M.set(0,0, 1.0)
    # M.set(0,1, 1.0)
    # table2 = osim.TimeSeriesTable([0.0],M,["name1", "name2"])
    # print("\nTable2")
    # print(table2)
    
    # marker reference :
    

    
    # InverseKinematicsSolver (const Model &model, 
    #MarkersReference &markersReference, 
    #SimTK::Array_< CoordinateReference > &coordinateReferences, 
    # double constraintWeight=SimTK::Infinity)
    
    
def getMarkersPositions(osimModel,osimState):
    markerSet = osimModel.getMarkerSet()
    D_markers = {}
    for marker in markerSet:
        name = marker.getName()
        position = numpy(marker.getLocationInGround(osimState) )
        D_markers[name] = position
    return(D_markers)

def getMarkersPositionsInLocalReferential(osimModel):
    markerSet = osimModel.getMarkerSet()
    D_markers = {}
    for marker in markerSet:
        name = marker.getName()
        position = numpy(marker.get_location() )
        D_markers[name] = position
    return(D_markers)    

def printMarkersPositions(osimModel,osimState, nameToo = False):
    D_markers = getMarkersPositions(osimModel, osimState)
    first = True

    for name in D_markers:
        pos = D_markers[name]
        if nameToo == False:
            if first : 
                print("{ "," {: .6f} ,  {: .6f} ,  {: .6f} ,".format(pos[0,0],pos[1,0],pos[2,0]) )
                first = False
            else:
                print("{: .6f} ,  {: .6f} ,  {: .6f} ,".format(pos[0,0],pos[1,0],pos[2,0]) )
        
        else:
            print("Name : {<24} | Position : [{: .4f} ,  {: .4f} ,  {: .4f}  ]".format(name,pos[0,0],pos[1,0],pos[2,0]))
    
    if nameToo == False:
        print(" };")
    
        
    
    

def getJacobian(osimModel,osimState,v=osim.Vec3(),n=0):
    """!
    
    Return a 6xNQ frame task Jacobian, for a frame task A fixed to a body B
    designated by an index n. 
    NQ is the size of the vector Q. The origin of the frame task A into the
    referential B is expressed by the vector v.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @param v : opensim.simbody.Vec3, optional.
        Origin of the frame task A into the referential B. The default is osim.Vec3().
        
    @param n : Int, optional.
        Mobilized Body Index. The body index to which the frame of interest
        is fixed. The default is 0.

    @return None.


    """

    smss = osimModel.getMatterSubsystem()
    J0 = osim.Matrix()

    # obtain Jacobian
    smss.calcFrameJacobian(osimState,n,v, J0)

    return(J0)


def tryForwardTool(osimModel):
    """!
    
    (BETA). Method used to try the Forward Tool of Opensim.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @return None.

    """
    osimModel.initSystem()

    forward = osim.ForwardTool()
    forward.setManager()
    forward.setModel(osimModel)
    forward.setName('test_forward')
    forward.setFinalTime(0.05)
    forward.run()

def setQ(osimModel,osimState,LQ):
    """!
    
    Method used to set the Q vector of a .osim Model, according to a list
    LQ.    

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @param LQ : List.
        List containing the values of each joint of the model.
            

    @return None.

    """
    sWorkingCopy = osimModel.updWorkingState()
    sWorkingCopy.setTime(osimState.getTime())
    osimModel.initStateWithoutRecreatingSystem(sWorkingCopy)
    Q = osimState.getQ()

    for k in range(len(LQ)):
        Q[k] = LQ[k]
    sWorkingCopy.setQ(Q)
    osimModel.realizePosition(sWorkingCopy)    

def setToNeutral(osimModel,osimState):
    """!
    
    Method used to set to neutral value all of the values of the articulations of the system.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return None.

    """

    coordSet = osimModel.updCoordinateSet()

    for k in range(coordSet.getSize()):
        coord = osimModel.updCoordinateSet().get(k)
        default = coord.getDefaultValue()
        coord.setValue(osimState,default,False)
    
    osimModel.assemble(osimState)

def setToRandom(osimModel,osimState):
    """!
    
    Method used to set to random values all of the values of the articulations of the system.

    @param osimModel : opensim.simulation.Model. 
        Object describing a .osim model, from the opensim module.

    @param osimState : opensim.simbody.State.
        Object describing the state of a .osim model, from the opensim module.

    @return None.

    """
    coordSet = osimModel.updCoordinateSet()

    for k in range(coordSet.getSize()):
        coord = osimModel.updCoordinateSet().get(k)
        min_r = coord.get_range(0)
        max_r = coord.get_range(1)
        rand = (max_r - min_r )*random.random() + min_r

        coord.setValue(osimState,rand,False)
    
    osimModel.assemble(osimState)



if __name__ == "__main__":
    
    # --- two_arms_only_marks.osim
    
    scaled_MM_Moved_path = '/home/elandais/ospi2urdf/models/two_arms_only/two_arms_only_marksScaledPlaced.osim'
    #'/home/elandais/ospi2urdf/models/two_arms_only/two_arms_only_marks.osim'
    setup_ik_path = '/home/elandais/opensim_python/OpenSim/XML/Walk_Mkrs_IK.xml'
    trc_path= '/home/elandais/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    geometry_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
    
    marker_file = "/home/elandais/opensim_python/OpenSim/MOT/Walk_Mkrs.mot"
    
    ik_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/InputFiles_module5_scaleIK"
            
    # # ---- wholebody.osim
    
    # scaled_MM_Moved_path = '/home/elandais/ospi/models/whole_body/wholebody.osim'
    
    # setup_ik_path = '/home/elandais/opensim_python/OpenSim/XML/Walk_Mkrs_IK.xml'
    # trc_path= '/home/elandais/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    # geometry_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
    
    # marker_file = "/home/elandais/opensim_python/OpenSim/MOT/Walk_Mkrs.mot"
    
    # ik_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/InputFiles_module5_scaleIK"
        
    # # ---- SubjectMoved.osim
    
    # scaled_MM_Moved_path = '/home/elandais/opensim_python/OpenSim/FullBodyModel/SubjectMoved.osim'  # Path to the subject Musculoskeletal Model that will be created
    
    # setup_ik_path = '/home/elandais/opensim_python/OpenSim/XML/Walk_Mkrs_IK.xml'
    # trc_path= '/home/elandais/opensim_python/OpenSim/TRC/Walk_Mkrs.trc'
    
    # geometry_path = "/home/elandais/opensim_python/OpenSim/FullBodyModel/Geometry"
    
    # marker_file = "/home/elandais/opensim_python/OpenSim/MOT/Walk_Mkrs.mot"
    
    # # ---- Arm26.osim
    
    # ## Complete path to the .osim file.
    # scaled_MM_Moved_path = '/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/arm26.osim'  # Path to the subject Musculoskeletal Model that will be created
    
    # ## Path to the folder containing Inverse Kinematics configuration file.
    # ik_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/"
    
    # ## Name of the Inverse Kinematics configuration file.
    # setup_ik_path = '/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/Setup_IK_arm26_elbow_flex.xml'
    
    # ## Path to a trc file associated with the .osim file.
    # trc_path= '/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm26/OutputReference/InverseKinematics/arm26_elbow_flex.trc'
    
    # ## Path to the .vtp files associated with the .osim model.
    # geometry_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
    
    # # --- MoBL_ARMS_module5_scaleIK_mod.osim

    ## Complete path to the .osim file.
    # scaled_MM_Moved_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/MoBL_ARMS_module5_scaleIK_mod.osim"  # Path to the subject Musculoskeletal Model that will be created
    
    # ik_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/InputFiles_module5_scaleIK"
    
    # ## Name of the Inverse Kinematics configuration file.
    # setup_ik_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/InputFiles_module5_scaleIK/MoBL_ARMS_module5_IK_Setup.xml"
    
    # ## Path to a trc file associated with the .osim file.
    # trc_path= "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/InputFiles_module5_scaleIK/reach.trc"
    
    # ## Path to the .vtp files associated with the .osim model.
    # geometry_path = "/home/elandais/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry"
  

    ## Constructor of the OsimModel class.
    OS_model = OsimModel(scaled_MM_Moved_path,geometry_path,ik_path,setup_ik_path,trc_path)
    
    #D_markers = getMarkersPositions(OS_model.osimModel, OS_model.osimState)
    
    time.sleep(0.1)
    #print(D_markers)
    setToNeutral(OS_model.osimModel,OS_model.osimState)
    
    printMarkersPositions(OS_model.osimModel,OS_model.osimState)
    printJointsValues(OS_model.osimModel,OS_model.osimState)
    
    #D = getMarkersByBodyParts(OS_model.osimModel)
    #print(D)
   
    # Dbody, D2 = getBodiesDistancesParentChild(OS_model.osimModel,OS_model.osimState)
    # print(D2)
    # setToRandom(OS_model.osimModel,OS_model.osimState)

    # printJointsValues(OS_model.osimModel,OS_model.osimState)
    D_body_markers = getMarkersByBodyParts(OS_model.osimModel)

    D2, D3 = reassembleByAxis(OS_model.osimModel,OS_model.osimState,D_body_markers)

    print(D2)
    
    # Lbody = list(D2.keys())
    # Lbody.remove('ulna_l')
    # Lbody.remove('ulna_r')
    
    # print(Lbody)
    
    # fixScalingFactors(OS_model.osimModel,OS_model.osimState,Lbody)
    #print(OS_model.D_jp)
    # markers_positions = [-0.0131,0.0395,0.1695,0.0126,-0.2974, 0.2000, -0.0131,-0.5336, 0.2514]
    
    # time.sleep(0.1)
    # IK_from_markers_positions(OS_model.osimModel, OS_model.osimState, markers_positions, setup_ik_path,trc_path)
    
    #ex_time_table = osim.TimeSeriesTable(trc_path)
