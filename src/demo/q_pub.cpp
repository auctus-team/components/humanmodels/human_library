#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
// PINOCCHIO_MODEL_DIR is defined by the CMake but you can define your own directory here.
#ifndef PINOCCHIO_MODEL_DIR
  #define PINOCCHIO_MODEL_DIR "/home/elandais/catkin_ws/human_library/description"
#endif

#include <OpenSim/OpenSim.h>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/JointState.h"
#include <sstream>
#include <cmath>

#include "std_srvs/SetBool.h"

/**
 * @brief Simple class testing the publication of Q vector to Rviz through JointState.
 * 
 */
class Q_move{
  public:
    /**
     * 
     * @param js_pub : Publisher of the JointState topic.
     * @param model : Pinocchio model object, representing the .urdf model.
     * @param joint_state : State machine vector, indicating the current state of each articulation.
     * @param q : Q state vector. 
     * 
     * @param dt : double, time step between two publications.
     * @param speed : double, speed of the articulations.
     * @param j_val : current value of the moved articulation.
     * 
     * 
     */
    ros::Publisher js_pub;
    pinocchio::Model model;
    std::vector<int> joint_state;  
    std::vector<double> q;

    double dt;
    double speed;

    double j_val;

    bool isPaused;    
    ros::ServiceServer pause_continue;

    Q_move(const std::string urdf_filename, double cons_speed, double name);
    void move_all_articulations(double & jl, int & i_joint);
    void publish_q();
bool ask_pause_continue(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res);

   

};
/**
 * @brief Construct a new q move::q move object
 * 
 * @param urdf_filename : string, path to the .urdf file.
 * @param cons_speed : double, desired speed of the articulations.
 * @param rate : double, publication rate of the JointState messages.
 */
Q_move::Q_move(const std::string urdf_filename, double cons_speed, double rate)
{
  ros::NodeHandle n;

  std::string urdf_file;
  std::string ext;
  std::string None = "None";

  n.param("/q_pub/urdf_path",urdf_file,urdf_filename);
  n.param("/q_pub/ext",ext,None);

  if (ext =="urdf")
  {
    urdf_file += ".urdf";
  }
  else if (ext == "xacro")
  {
    urdf_file += ".urdf.xacro";
  };

  std::cout << "path : " << urdf_file << std::endl;

  pinocchio::urdf::buildModel(urdf_file,model);
  
  for(int i = 1; i< model.names.size(); i++){
    std::cout << "names : " << model.names[i] << std::endl;
    joint_state.push_back(0);
    q.push_back(0.0);
  }
  pause_continue = n.advertiseService("/mocap_node/pause_continue",  &Q_move::ask_pause_continue,this);
  isPaused = false;

  js_pub = n.advertise<sensor_msgs::JointState>("Q_pub", 1000);

  ros::Rate lr(rate);

  dt = 1.0/rate;
  speed = cons_speed;

  double j_val = 0.0;
  int i_joint = 1;

  while(ros::ok())
  {
    if (!isPaused)
    {
    move_all_articulations(j_val,i_joint);
    }

   ros::spinOnce();
   lr.sleep();
  };

}


bool Q_move::ask_pause_continue(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res)
{


  isPaused = req.data;

  res.success = true;
  res.message = "pause_continue ok";

}

/**
 * @brief Move a specific articulation i_joint, and updates its value j_val. 
 * This j_val value is updated according to a state machine, whose states are : 
 * - 0 : initialization of the articulation.
 * - 1 : change from j_val = 0 to j_val = maximum limit of the joint.
 * - 2 : change from j_val = maximum limit to j_val = minimum limit.
 * - 3 : change from j_val = minimum limit to j_val = 0.
 * - 4 : end of the movement of the joint.
 * 
 * @param j_val : double, current value of the joint.
 * @param i_joint : int, index of the joint.
 */
void Q_move::move_all_articulations(double & j_val,int & i_joint)
{

double jl;
int idx_q;

if (i_joint < model.names.size() )
{
  //int nq =  model.joints[i_joint].nq;
  //std::cout << model.joints[i_joint] << std::endl;
  idx_q = model.joints[i_joint].idx_q_impl();

  if (joint_state[i_joint-1] == 0)
  {
    j_val = 0.0;
    joint_state[i_joint-1] = 1;

  }

  else if (joint_state[i_joint-1] == 1)
  {
    jl = model.upperPositionLimit[idx_q];
    j_val += speed*dt ;
    //ROS_INFO("j_val : %f",j_val);
    j_val = std::min(j_val,jl);
    //ROS_INFO("j_val aft : %f",j_val);
    if (j_val == jl)
    {
      joint_state[i_joint-1] = 2;

    };

  }

  else if (joint_state[i_joint-1] ==  2)
  {
    jl = model.lowerPositionLimit[idx_q];
    j_val -= speed*dt ;
    j_val = std::max(j_val,jl);

    if (j_val == jl)
    {
      joint_state[i_joint-1] = 3;

    };

  }

  else if (joint_state[i_joint-1] ==  3)
  {
    jl = 0.0;
    j_val += speed*dt ;
    j_val = std::min(j_val,jl);

    if (j_val == jl)
    {
      joint_state[i_joint-1] = 4;

    };

  };


  
  
  q[idx_q] = j_val;
  publish_q();

  if (joint_state[i_joint-1]==4)
  {
    i_joint+=1;
  };

  //ROS_INFO("i_joint : %d; joint_state_assos : %d; j_val : %f; idx_q : %d",i_joint,joint_state[i_joint],j_val,idx_q);

}
else
{
  std::cout << "--- Reboot movements ---"  << std::endl;
  i_joint = 1;
  j_val = 0.0;
  for (int i =0; i< joint_state.size(); i++)
  {
    joint_state[i] = 0;
  };
};

};
/**
 * @brief Publish the q vector of the model, as a JointState message.
 * 
 */
void Q_move::publish_q()
{
    sensor_msgs::JointState js;

    js.header.stamp = ros::Time::now();
    js.header.frame_id = "ground";

    for(int i = 1; i< model.names.size(); i++){
      js.name.push_back(model.names[i]);
      js.position.push_back(q[i-1]);

    }

    js_pub.publish(js);

};

/**
 * @brief Main method of the program. Creates the Q_move object.
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char **argv)
{

  //const std::string urdf_filename = "/home/elandais/catkin_ws/src/human_library/description/pendulum/pendulum.urdf.xacro";


  // ---- wholebody

  const std::string urdf_filename ="/home/elandais/catkin_ws/src/human_library/description/wholebody/wholebody_aCol.urdf.xacro";

  // ---- arm26

  //const std::string urdf_filename = "/home/elandais/catkin_ws/src/human_library/description/arm26/arm26_aCol.urdf.xacro";

   double speed = M_PI_2/3;
   double rate = 10.0;

   ros::init(argc,argv,"q_pub");

   Q_move q_move(urdf_filename,speed,rate);

  return 0;
}