#ifndef fosim_h
#define fosim_h

#include <typeinfo>
/* #include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
// PINOCCHIO_MODEL_DIR is defined by the CMake but you can define your own directory here.
#ifndef PINOCCHIO_MODEL_DIR
  #define PINOCCHIO_MODEL_DIR "/home/elandais/catkin_ws/human_control/description"
#endif
 */
#include <OpenSim/OpenSim.h>
#include <OpenSim/Common/MarkerData.h>

#include <OpenSim/Simulation/Model/Model.h>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/JointState.h"
#include <sstream>
#include <cmath>
#include <map>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

namespace osim = OpenSim;


/**
 * @brief Simple class testing the publication of Q vector to Rviz through JointState.
 * 
 */
namespace fosim{

    class OsimModel{
    public:
        /**
         * 
         * @param js_pub : Publisher of the JointState topic.
         * @param model : Pinocchio model object, representing the .urdf model.
         * @param joint_state : State machine vector, indicating the current state of each articulation.
         * @param q : Q state vector. 
         * 
         * @param dt : double, time step between two publications.
         * @param speed : double, speed of the articulations.
         * @param j_val : current value of the moved articulation.
         * 
         * 
         */

        std::string model_path;
        std::string geometry_path;
        std::string setup_ik_file;

        osim::Model osimModel;
        SimTK::State osimState;
        // Map object coordinate_name:value_of_coordinate
        std::map<std::string, double> D_joints_values;
        // Map object marker_name:position_of_marker_in_ground
        std::map<std::string, Eigen::Vector3d> D_mark_pos;
        // Map object marker_name:position_of_marker_in_local_referential
        std::map<std::string, Eigen::Vector3d> D_mark_pos_local;
        // Map object body_name:names_of_body_markers
        std::map<std::string, std::vector<std::string> > D_body_markers;
        // Map object marker_name:name_of_body_attached
        std::map<std::string, std::string > D_markers_body;
        // Map object index_on_q_vector:index_on_coordinateSet
        std::map<int, int> D_Q_coord;
        // Map object index_on_coordinateSet:index_on_q_vector
        std::map<int, int> D_coord_Q;
        // Map object name_of_transform:index_on_coordinateSet, for ground_joint
        std::map<std::string,int> D_ground_props;
        // Map object that have, for each body child, the type of transform
        // between the body_child and the body_parent, and the index of the 
        // coordinate concerned by this transform in coordinateSet
        std::map<std::string,std::map<std::string,int> > D_body_child_transfo_props;
        // Map object child_name:parent_name
        std::map<std::string, std::string> D_child_parent;

        std::map<std::string, double> D_markers_weight;

        std::map<std::string, std::vector<std::string> > D_parent_all_children;

        // Map object joint_name:transform_between_body_parent_and_body_child
        std::map< std::string , SimTK::Transform > D_jp;
        std::map< std::string , double > D_mark_dist;
        std::map< std::string , double > D_mark_closest_dist;
        std::map< std::string , std::map<std::string,double> > D_mark_all_dist;

        std::map<int,int> D_alph_multi;

        std::vector<std::string> markers_bodies_multiorder;

        double min_mark_dist;
        double max_mark_dist;
        Eigen::Vector3d lowest_mark;

        OsimModel();
        void fromScratch();
        void clone3(OsimModel &, bool setQ=true,bool setMarkers=false);

        OsimModel(std::string model_path_, std::string geometry_path_, std::string setup_ik_file_="");
        void updModel();
        void totalUpdModel();
        void setQ(std::vector<double> LQ, int Qtyp = 1);
        void setQ(std::map<std::string,double> D_joints_values);

        void setToNeutral();
        void setMarkersPositions(std::map<std::string,Eigen::Vector3d> markersLocation,
        std::string referential = "ground");
        void setGroundJointOnly(Eigen::Matrix4f transf);
        void applyStretching(std::map<std::string,Eigen::Vector3d> & markersLocation, std::map<std::string,std::vector<double> > stretching_vector, std::string referential="local" );
        void applyStretching2(std::map<std::string,Eigen::Vector3d> & markersLocation, std::map<std::string,std::vector<double> > stretching_vector );

        void setAndCorrectCoordVec(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> & LQ,  int correct_type = 1);

        int IK(std::map<std::string,Eigen::Vector3d> markers_positions, 
        std::map<std::string,double> markers_weights, int mode, double time,
        std::map<std::string,double> coordinates_weights = std::map<std::string,double>() );
    };


    void getPropertiesOfEachJoint(osim::Model & osimModel, SimTK::State & osimState);

    void setQWithTransform2(osim::Model & osimModel, SimTK::State & osimState);

    void setQWithTransform(osim::Model & osimModel, SimTK::State & osimState);

    void printChildParentBody(osim::Model & osimModel);

    std::map<std::string,std::string> getChildParentBody(osim::Model & osimModel);

    void IKFromMarkersPositions(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> markers_positions, std::string xml_taskset, std::string trc_taskset);

    void printJointsValues(osim::Model & osimModel, SimTK::State & osimState);

    std::map<std::string, double> getJointsValues(osim::Model & osimModel, SimTK::State & osimState);
    
    void printMarkersPositions(osim::Model & osimModel, SimTK::State & osimState);
    void printMarkersPositions(std::map<std::string, Eigen::Vector3d> D_markers_positions);

    std::map<std::string, Eigen::Vector3d> getMarkersPositions(osim::Model & osimModel, SimTK::State & osimState, std::string refe="Ground");

    void setAndCheckCoordVec(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> & LQ,  int correct_type = 1);

    void setQVec(osim::Model & Model, SimTK::State & osimState, std::vector<double> q);

    void setToNeutral(osim::Model & Model, SimTK::State & osimState);

    std::map<int, int> getCorresQCoord(osim::Model & osimModel,SimTK::State & osimState);

    std::map<int, int> getCorresCoordQ(osim::Model & osimModel,SimTK::State & osimState);

    std::map<int, int> getCorresAlphMulti(osim::Model & osimModel,SimTK::State & osimState);

void printCorresAlphMulti(osim::Model & osimModel,SimTK::State & osimState);

    void getDistanceBetweenMarkers(osim::Model & osimModel, SimTK::State & osimState, double & min_dist, double & max_dist);

    Eigen::Vector3d getLowestOsimMarker(osim::Model & osimModel, SimTK::State & osimState);

   Eigen::Vector3d getLowestMarker(std::vector<Eigen::Vector3d> positions);

  std::map<std::string, std::vector<std::string> > getMarkersByBodyParts(osim::Model & osimModel,SimTK::State & osimState);
  
  std::map<std::string, std::string > getBodyByMarkers(osim::Model & osimModel,SimTK::State & osimState);
  
  void printMarkersByBodyParts(osim::Model & osimModel,SimTK::State & osimState);
 
  std::vector<std::string> splitString(std::string str, std::string delimiter);

std::map<std::string,int> getGroundProperties(osim::Model & osimModel, SimTK::State & osimState);

void printGroundProperties(osim::Model & osimModel, SimTK::State & osimState);

std::map< std::string, SimTK::Transform > getJointsTransform(osim::Model & osimModel, SimTK::State & osimState, std::string refe = "Ground", int n=1);

std::map<std::string, double> getClosestMarkerFromOtherBody(osim::Model & osimModel, SimTK::State & osimState);
std::map<std::string, double> getClosestMarkerFromOtherBody(osim::Model & osimModel, std::map<std::string, Eigen::Vector3d> D_pos);

std::map<std::string, double> getClosestMarkerInBody(osim::Model & osimModel, SimTK::State & osimState);

std::map<std::string, std::map<std::string,double> > getMarkerDistancesInBody(osim::Model & osimModel, SimTK::State & osimState);
std::map<std::string, std::map<std::string,double> > getMarkerDistancesInBody(OsimModel & osMod, std::map<std::string, Eigen::Vector3d> markers_positions);

int IKFromMarkersPositionsAndWeights(osim::Model & osimModel, SimTK::State & s, 
std::map<std::string,Eigen::Vector3d> markers_positions, 
std::map<std::string,double> markers_weights,
std::map<std::string,double> coordinates_weights,
int mode,
double time);

void printMarkerDistancesInBody(OsimModel & osMod, std::map<std::string, Eigen::Vector3d> markers_positions);
void printMarkerDistancesInBody(osim::Model & osimModel, SimTK::State & osimState);

void writeTRCFile(std::string filename, std::map< double, std::map<std::string, Eigen::Vector3d>  > TRCData, int dataRate);
 
void writeMOTFile(std::string filename, std::map< double, std::map<std::string, double>  > MOTData);

  template<typename T, typename U>
  void toStdVector(const T& srcArray, U& dstVector);

    void readTRCFile(const std::string & filename, 
  std::map< double, std::map<std::string, Eigen::Vector3d>  > & TRCData,
  int & dataRate,
  int & numMarkers);


  void getMarkersBodiesInMultibodyOrder(std::map<std::string,std::string> D_child_parent,
    std::map<std::string,std::vector<std::string> > D_body_markers,
     std::vector<std::string> & body_in_multiorder,
     std::map<std::string, std::vector<std::string> > & D_parent_all_children);
    
  void getMarkersBodiesInMultibodyOrder(osim::Model & osimModel, SimTK::State & osimState,
       std::vector<std::string> & body_in_multiorder,
     std::map<std::string, std::vector<std::string> > & D_parent_all_children);
    
    
  std::map<std::string,double> computeMarkersWeights(osim::Model & osimModel, SimTK::State & osimState);

    std::map<std::string,double> computeMarkersWeights(std::vector<std::string> markers_bodies_multiorder,
    std::map<std::string,std::string> D_child_parent,
  std::map<std::string,Eigen::Vector3d> D_mark_pos_local,
  std::map<std::string,std::vector<std::string> > D_bodies_markers);

 Eigen::Matrix4f getBodyTransformInGround(osim::Model & osimModel, SimTK::State & osimState, std::string body_name);

void printMarkersWeights(std::map<std::string,double> markersWeights);


}

#endif