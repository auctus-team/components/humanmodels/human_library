#ifndef fpin_h
#define fpin_h

#include <typeinfo>
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/geometry.hpp"
#include "pinocchio/algorithm/crba.hpp"
#include "pinocchio/algorithm/rnea.hpp"
// PINOCCHIO_MODEL_DIR is defined by the CMake but you can define your own directory here.
#ifndef PINOCCHIO_MODEL_DIR
  #define PINOCCHIO_MODEL_DIR "/home/elandais/catkin_ws/human_control/description"
#endif
 
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/JointState.h"
#include <sstream>
#include <cmath>
#include <map>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

namespace pin = pinocchio;

/**
 * @brief Simple class testing the publication of Q vector to Rviz through JointState.
 * 
 */
namespace fpin{

    class URDFModel{
    public:
        /**
         * 
         * @param js_pub : Publisher of the JointState topic.
         * @param model : Pinocchio model object, representing the .urdf model.
         * @param joint_state : State machine vector, indicating the current state of each articulation.
         * @param q : Q state vector. 
         * 
         * @param dt : double, time step between two publications.
         * @param speed : double, speed of the articulations.
         * @param j_val : current value of the moved articulation.
         * 
         * 
         */
        pin::Model urdf_model;
        pin::Data urdf_data;
        Eigen::VectorXd q;
        Eigen::VectorXd v;

        std::map<std::string, std::vector<int> > D_joints_props;

        URDFModel();
        URDFModel(std::string urdf_filename);
        void setNeutral();
        void forwardKinematics(std::map<std::string,std::vector<double> > D_j_v);
        void setSpeed(std::map<std::string,std::vector<double> > D_j_s);
    };

    void printJointsPositions( pin::Model urdf_model, pin::Data urdf_data);

    void printJointsValues(pin::Model urdf_model, Eigen::VectorXd q);
  
    void printJointsSpeeds(pin::Model urdf_model, Eigen::VectorXd v);
    std::map<std::string, std::vector<int> > getJointsProperties(pin::Model urdf_model);

    std::map<std::string,Eigen::Matrix4d> getJointsPositions(pin::Model urdf_model, pin::Data urdf_data);
    
    std::map<std::string,Eigen::Matrix4d> getFramesPositions(pin::Model urdf_model, pin::Data urdf_data);

    void printJointsProperties(pin::Model urdf_model);


}

#endif